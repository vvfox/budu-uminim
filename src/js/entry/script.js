import mainWork from '../page_scripts/main';
import axios from 'axios' ;

function validate_num(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode( key );
    var regex = /[0-9]|\./;
    if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }
}

(function(w){
    "use strict";
    class Ajax{
        //Упрощаем работу с ajax-обращениями
        constructor(dir){
            this.ajaxConf = {
                path : "/api/" + dir + '.php',
                dataType : 'json',
                method: 'POST'
            };
        }
        send (action, data, fn){
            // alert('ok');


            let oSend = {
                action: action,
                data:data

            };

            axios.post(this.ajaxConf.path, oSend)
                .then(respon => {

                    // this.info = response
                    if(typeof fn === "function") fn(respon.data);
                    else console.log('not set function');
                    // console.log(respon);

                })
                .catch((error) => {
                    console.log(error);
//                    console.log(resp);
//                     this.errored = true;
                })
                .finally(() => {

                //this.loading = false

                    console.log('finally');

            });
        }
        validateEmail (email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }
    }
    window.Ajax = Ajax;
    window.number_format = function ( number, decimals, dec_point, thousands_sep ) {	// Format a number with grouped thousands
        let i, j, kw, kd, km;
        // input sanitation & defaults
        if( isNaN(decimals = Math.abs(decimals)) ){
            decimals = 2;
        }
        if( dec_point == undefined ){
            dec_point = ",";
        }
        if( thousands_sep == undefined ){
            thousands_sep = ".";
        }
        i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

        if( (j = i.length) > 3 ){
            j = j % 3;
        } else{
            j = 0;
        }
        km = (j ? i.substr(0, j) + thousands_sep : "");
        kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
        //kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).slice(2) : "");
        kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");
        return km + kw + kd;
    };


    $(function () {

        if($('#app').length) {
            mainWork();
        }
        let $auth_frm = $('#auth-form'),
            oAjax = new Ajax('user');
        if($auth_frm.length){
            let $submit = $auth_frm.find('input[type="submit"]'),
                $email = $auth_frm.find('input.email'),
                $r_message =  $auth_frm.find('.f-row-message'),
                $end_result = $auth_frm.find('.frm-message');


            $submit.on('click', function(){
                const email = $email.val();
                $r_message.html('');
                if(email.length){

                    if(oAjax.validateEmail(email)){
                        oAjax.send('auth', {email: email}, function(dt){


                            console.log(dt);
                            if('error' in dt){
                                $r_message.html(dt.error);
                            }else{
                                $auth_frm.addClass('sended');
                                if('message' in dt) $end_result.html(dt.message);
                            }
                            console.log(dt);


                            // location.reload();
                        });
                    }else{
                        $r_message.html("Укажите верный e-mail");
                    }

                }else{
                    $r_message.html('Укажите e-mail');
                }
                return false;
            });
        }
    });

    $(function () {




        // if($('.p-chains').length){
        //     chainsWork();
        // }
        // if($('.p-order').length){
        //     ordersWork();
        // }

        $('.b-pluse-minus button').click(function(){
            let $this = $(this),
                $input = $this.closest('.b-pluse-minus').find('input[type="text"]'),
                val = parseInt($input.val());
            if($(this).hasClass('pluse')){
                val++;
            } else{
                val--;
            }
            $input.val(val);
            $input.change();
        });
        $('.b-pluse-minus input').keypress(validate_num);

    });


}( typeof global !== "undefined" ? global : this ));

