import Vue from 'vue';
import VueRouter from 'vue-router';
import store from './store';
import axios from 'axios' ;
import app from './app.vue';
// import SchoolList from '../../components/schols-list';
import SchoolDetail from '../../components/schools/detail/index.vue';
import SchoolList from '../../components/schools/list/index.vue';
import SchoolEdit from '../../components/schools/edit/index.vue';
import SchoolGroups from '../../components/schools/edit/index.vue';
import SchoolAdminAdd from '../../components/user/edit/index.vue';



import GroupEdit from '../../components/groups/edit/index.vue';
import GroupDetail from '../../components/groups/detail/index.vue';
import TaskEdit from '../../components/task/edit/index.vue';
import TaskDetail from '../../components/task/detail/index.vue';

import MainPage from '../../components/main/index.vue';
import TeachGroup from '../../components/groups/teach-group/index.vue';
import Task from '../../components/task/child/index.vue';

/* User */
import UserList from '../../components/user/list/index.vue';
import UserDetail from '../../components/user/detail/index.vue';
import UserEdit from '../../components/user/edit/index.vue';

Vue.use(VueRouter);
export default function(){
    (function renderApp() {
        const Auth = {
            template: '<div>Авторизоваться</div>'
        };
        const Tasks = {
            template: '<div>Задания</div>'
        };
        const router = new VueRouter({
            mode: 'history',
            routes: [

              //
                {path:'/auth/', component: Auth,name:'auth'},

                /* School */
                {path:'/schools/',              component: SchoolList, name: 'school-list'},
                {path:'/schools/add',           component: SchoolEdit, name: 'school-add' },
                {path:'/schools/:id_school/edit',      component: SchoolEdit, name: 'school-edit' , props: true},
                {path:'/schools/:id_school/add-admin',    component: UserEdit, name: 'add-admin',   props: true},
                {path:'/schools/:id_school/groups',    component: SchoolList, name: 'school-groups',  props: true},
                {path:'/schools/:id_school/groups/add',    component: GroupEdit, name: 'group-add',   props: true},
                {path:'/schools/:id_school/groups/:id_group',  component: GroupDetail, name: 'group-detail',   props: true},
                {path:'/schools/:id_school/groups/:id_group/edit',  component: GroupEdit, name: 'group-edit',  props: true},
                {path:'/schools/:id_school/groups/:id_group/add-child',  component: UserEdit, name: 'group-add-child',   props: true},
                {path:'/schools/:id_school/groups/:id_group/user/:id',  component: UserDetail, name: 'group-child',   props: true},
                {path:'/schools/:id_school/groups/:id_group/user/:id/edit',  component: UserEdit, name: 'group-edit-child',   props: true},
                {path:'/schools/:id_school/groups/:id_group/task/add',  component: TaskEdit, name: 'task-add',   props: true},
                {path:'/schools/:id_school/groups/:id_group/task/:id_task',  component: TaskDetail, name: 'task-detail',   props: true},
                {path:'/schools/:id_school/groups/:id_group/task/:id_task/edit',  component: TaskEdit, name: 'task-edit',   props: true},

                {path:'/schools/:id_school',           component: SchoolDetail, name: 'school-detail',   props: true},


                {path:'/groups/', component: TeachGroup, name:'teach-group'},
                {path:'/task/', component: Task, name:'task'},

                /* Group */
                // {path:'/groups/', component: Groups, name:'group-list'},
                /* User */
                {path: '/users/', component: UserList, name:'user-list' },
                {path: '/users/add', component: UserEdit, name:'user-add' },
                {path: '/users/:id/edit', component: UserEdit, name:'user-edit',  props: true },
                {path: '/users/:id', component: UserDetail, name:'user-detail',  props: true },

                /* Task */
                // {path:'/tasks/', component: Tasks, name:'task-list'},
                {path: '/',     component: MainPage, name:'main-page'}
            ]
        });


        let appVue = new Vue({
            el: '#app',
            render: h => h(app),
            store,
            router
        });
    }());

}