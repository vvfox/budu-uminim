<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

require_once $_SERVER['DOCUMENT_ROOT'].'/define/options.php';

if(isset($_GET['correct'])){

	$s_correct = trim($_GET['correct']);

	if($s_correct!=''){
		$auth_result = $user->auth($s_correct);
		if(isset($auth_result['error'])){
			$_ERRORS[]  = $auth_result['error'];
		}else{
		    header('Location: /auth/');
        }
	}
}
require_once $_SERVER['DOCUMENT_ROOT'].'/'.CMS_PATH.'/header.php';
?>
<?if($user->is_auth()){?>
	<div class="full-page">
        <form id="auth-form" class="frm frm-auth center-frm-block">
            <h1 class="h1">Вы успешно авторизованы.</h1>
            <p>Через несколько секунд вы будете перенаправлены на основной экран.</p>
            <script>
                window.onload = function(){
                    setTimeout(() =>{
                        location.href = '/';
                    }, 3000);
                }
            </script>
        </form>
	</div>
<?}?>
<? require_once $_SERVER['DOCUMENT_ROOT'].'/'.CMS_PATH.'/footer.php';?>