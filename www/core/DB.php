<?php
define('DB_HOST', 'localhost');

if($_SERVER['HTTP_HOST'] === 'budu.loc'){
	define('DB_NAME', 'db_budu');
	define('DB_USER', 'root');
	define('DB_PASS', '23068919');
	define('DB_CHAR', 'utf8');
}else{
	define('DB_NAME', 'db_budu');
	define('DB_USER', 'db_budu');
	define('DB_PASS', '7Z8m7D3g');
	define('DB_CHAR', 'utf8');
}



class DB
{
	protected static $instance = null;

	public function __construct() {}
	public function __clone() {}

	public static function instance()
	{
		if (self::$instance === null)
		{
			$opt  = array(
				PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
				PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
				PDO::ATTR_EMULATE_PREPARES   => TRUE,
			);
			$dsn = 'mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset='.DB_CHAR;
			self::$instance = new PDO($dsn, DB_USER, DB_PASS, $opt);
		}
		return self::$instance;
	}

	public static function __callStatic($method, $args)
	{
		return call_user_func_array(array(self::instance(), $method), $args);
	}

	public static function run($sql, $args = [])
	{
		if (!$args)
		{
			return self::instance()->query($sql);
		}
		$stmt = self::instance()->prepare($sql);
		$stmt->execute($args);
		return $stmt;
	}

}