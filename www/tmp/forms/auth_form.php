<div class="full-page">


	<form id="auth-form" class="frm frm-auth center-frm-block">
	<h1 class="h1 logo">БудУмныМ.ру</h1>
                    <div class="desc">тренажер навыков ментальной арифметики</div>
		<h2 class="h2">Авторизация</h2>
		<div class="frm-content">
			<div class="f-row">
				<input type="text" class="email" name="email" placeholder="введите e-mail">
			</div>
			<div class="f-row-message"></div>
			<div class="f-row">
				<input type="submit" class="btn" id="auth" value="Войти">
			</div>
		</div>
		<div class="frm-message">
			<h2 class="h2 title"></h2>
			<p class="text"></p>
		</div>
        <div class="f-row">
            <a href="/training/">Открыть тренажер</a>
        </div>
	</form>
</div>
