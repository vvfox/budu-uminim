<?php
/**
 * Created by PhpStorm.
 * User: vvzakharov
 * Date: 28.08.2018
 * Time: 15:44
 */


Class App{
	private $JS = array();
	private $CSS = array();



	public function AddHeadScript($link){
		$this->JS[] = $link;
	}
	public function SetAdditionalCSS($link){
		$this->CSS[] = $link;
	}
	public function ShowHead(){
		foreach ($this->JS as $js){
			echo "<script src=\"$js\"></script>";

		}
		foreach ($this->CSS as $css){
			echo "<link  href=\"$css\" type='text/css' rel=\"stylesheet\">";

		}
	}
	public function GetCurDir(){
		$full_path = getcwd();
		$root_path = $_SERVER['DOCUMENT_ROOT'];
		if(strpos($full_path, "/") === false)
			$root_path = str_ireplace('/', '\\', $root_path);

		return (strlen($root_path)<strlen($full_path)?
			"/".str_ireplace($root_path, "", $full_path)."/":""
		);
	}
}
$APPLICATION = new App();
define('SITE_TEMPLATE_PATH', '/local/templates/.default');
define('LANGUAGE_ID', 'ru');
define('B_PROLOG_INCLUDED', true);



include __DIR__.'/..'.SITE_TEMPLATE_PATH.'/header.php';


//eche>';o '<pre>';
//print_r($arUser);
//print_r($arProject);
//print_r($arChains);
//echo '</pr