<?php
/**
 * Created by PhpStorm.
 * User: vvzakharov
 * Date: 19.09.2018
 * Time: 21:30
 */
class cUsers {
	private $t_user = 't_users';
	private $t_tokens = 't_tokens';
	private $t_groups_users = "t_groups_users";
	private $school;
	public $ar_groups  = [
		'1'=> 'Администратор центра',
		'2'=> 'Администратор школы',
		'3'=> 'Преподаватель',
		'4'=> 'Ученик'
	];
	private $helper;
	function __construct() {
		$this->helper = new cHelper();
		$this->school = new cSchool();
	}

	public function getGroups(){

		return $this->ar_groups;
	}

	public function auth($token){

		$sqlFndByToken = "SELECT * FROM $this->t_tokens WHERE correct LIKE :token";
		$row = DB::run($sqlFndByToken, ['token'=>$token])->fetch();
		if(!$row){
			return array('error'=>'not found user');
		}
		$user_data = $this->getByID($row['id_user']);

		if($user_data['active']==1){
			$school_id_blocked = false;
			if($user_data['id_school']!==""){
				$school_data = $this->school->getByID($user_data['id_school']);
				print_r($school_data);


				if($school_data['active']!='1'){
					$school_id_blocked = true;
				}
			}
			if(!$school_id_blocked){
				$_SESSION['user'] = $row['id_user'];
				return $row;
			}else{
				return ['error'=>'Ваша школа заблокирована'];
			}

		}else{
			return ['error'=>'Пользователь заблокирован'];
		}


	}
	public function logout(){
//		$_SESSION "user", "");
		unset($_SESSION['user']);
	}
	public function getID(){
		if(isset($_SESSION['user'])){
			return $_SESSION['user'];
		}

	}
	public function getListByGroupType($id_group, $type){

		$sql = "SELECT  * FROM $this->t_groups_users".
		" LEFT JOIN $this->t_user ON  $this->t_groups_users.id_user = $this->t_user.id_user ".
		" WHERE $this->t_groups_users.id_group = :id_group AND $this->t_user.type=:type";

		return DB::run($sql, [
			'id_group'=>$id_group,
			'type'=>$type

		]);


	}


	public function getListBySchoolType($id_school, $type){

		$arUserList = $this->helper->getList($this->t_user, ['where'=>[
			'id_school'=>$id_school,
			'type'=>$type
		]]);
		return $arUserList;

	}
	public function getList(){
		$arUserList = $this->helper->getList($this->t_user);
		return $arUserList;
	}
	public function getByID($id){
		$row = DB::run("SELECT * FROM $this->t_user WHERE id_user = :id_user", ['id_user'=>$id])->fetch();
		if(!$row){
			return array('error'=>'Пользователь не найден');
		}
		return $row;
	}

	public function up_auth(){
		if(isset($_SESSION['user'])){
			$id_user = $_SESSION['user'];
			$_SESSION['user'] = $id_user;
			return true;
		}
		else return false;

	}
	public function is_auth(){
		if(isset($_SESSION['user'])){
			$id_user = $_SESSION['user'];
//			setcookie('user', $id_user, time()+(24*3600));
			return true;
		}
		else return false;
	}
	function isValidEmail($email){
		return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
	}
	public function fndByEmail($email){
		$sql ="SELECT * FROM $this->t_user WHERE email LIKE :email";
		$row = DB::run($sql, ['email'=>$email])->fetch();
		if(!$row){

			$arReturn  =array('error'=>'Адрес электронной почты не найден.');
			$arReturn['sql'] = $sql;
			$arReturn['data'] = $email;
			return $arReturn;
		}
		return $row;
	}
	public function create_token($aUser){
		$id_user = $aUser['id_user'];
		$date = new DateTime();
		$timestamp = $date->getTimestamp();
		$correct_str = sha1($id_user.$timestamp);
		$secure_connection = false;
		if(isset($_SERVER['HTTPS'])) if ($_SERVER['HTTPS'] == "on") $secure_connection = true;


		$str_insert = "INSERT INTO $this->t_tokens VALUES (:id_user, :dt, :correct, :success)";


//		$user_data = $this->getByID()
		DB::run($str_insert, array(
			'id_user'=>$id_user,
			'dt'=>$timestamp,
			'correct'=>$correct_str,
			'success'=>'0'
		));
		$auth_link =($secure_connection?'https://':'http://').$_SERVER['HTTP_HOST'].'/auth/?correct='.$correct_str;

		mail($aUser['email'], 'Вход в Буду умным', 'Для авторизации на сайте '.$_SERVER['HTTP_HOST'].' перейдти по одноразовой ссылке '.$auth_link);


		$arReturn = [
			'message'=>'На указанный адрес электронной почты отправлена ссылка для авторизации.',

		];
		if($_SERVER['HTTP_HOST']=='budu.loc'){
			$arReturn['link'] = $auth_link;
		}
		return $arReturn;

	}
	public function get_token($email){
		if($email!==''){
			if($this->isValidEmail($email)){
				$fnd = $this->fndByEmail($email);
				if(!isset($fnd['error'])){

					if($fnd['active']==1)
						return $this->create_token($fnd);
					else
						return ['error'=>'Данный пользователь был заблокирован'];
				}else{
					return $fnd;
				}
			}else{
				return array('error'=>'Неверный адрес электронной почты');
			}
		}else{
			return array('error'=>'Не указан адрес электронной почты');
		}
	}
	public function add($user=array()){

//		$sql = "INSERT INTO t_users (`email`, `f_name`, `l_name`, `s_name`, `type`) VALUES (:email, :f_name, :l_name, :s_name, :type);";
		$id_user = $this->helper->insert($this->t_user, $user);
		return $this->getByID($id_user);
		//INSERT INTO t_users (`email`, `name`, `type`, `id_project`) VALUES ('test@test.ru', 'test', '2', '1');

	}
	public function edit($id_user, $user=array()){
		$sql = "UPDATE $this->t_user SET ";
		$arSet = [];
		foreach($user as $field=>$value){
			$arSet[]="$field = :$field";
		}
		$sql.=implode(', ', $arSet)." WHERE id_user = :id_user";
		$user['id_user'] = $id_user;
		DB::run($sql, $user);
		return $user;
	}
	public function delete($email=''){

	}
	public function setGroupUser($id_user, $id_group){
		$this->helper->delete($this->t_groups_users, ['id_user'=>$id_user]);
		return $this->helper->insert($this->t_groups_users, [
			'id_user'=>$id_user,
			'id_group'=>$id_group
		]);
	}
}
