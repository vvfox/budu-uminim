<?php
/**
 * Created by PhpStorm.
 * User: vvzakharov
 * Date: 25/11/2018
 * Time: 21:09
 */


class cTask{
	private $tb = "t_task";
	private $tb_task_user = "t_task_user";
	private $tb_task_user_answer = "t_task_user_answer";
	public $rules = [
		'five' => [
			['val'=> "five_-4", "name"=> "-4 (-5+1)"],
			['val'=> "five_-3", "name"=> "-3 (-5+2)"],
			['val'=> "five_-2", "name"=> "-2 (-5+3)"],
			['val'=> "five_-1", "name"=> "-1 (-5+4)"],
			['val'=> "five_1",  "name"=>  "1 (-4+5)"],
			['val'=> "five_2",  "name"=>  "2 (-3+5)"],
			['val'=> "five_3",  "name"=>  "3 (-2+5)"],
			['val'=> "five_4",  "name"=>  "4 (-1+5)"],
		],
		"ten"=>[
			["val"=> "ten_1" , "name"=> "+1 (-9+10)"],
			["val"=> "ten_2" , "name"=> "+2 (-8+10)"],
			["val"=> "ten_3" , "name"=> "+3 (-7+10)"],
			["val"=> "ten_4" , "name"=> "+4 (-6+10)"],
			["val"=> "ten_5" , "name"=> "+5 (-5+10)"],
			["val"=> "ten_6" , "name"=> "+6 (-4+10)"],
			["val"=> "ten_7" , "name"=> "+7 (-3+10)"],
			["val"=> "ten_8" , "name"=> "+8 (-2+10)"],
			["val"=> "ten_8" , "name"=> "+9 (-1+10)"],

			["val"=> "ten_-9"  , "name"=> "-9 (-10+1)"],

			["val"=> "ten_-8"  , "name"=> "-8 (-10+2)"],
			["val"=> "ten_-7"  , "name"=> "-7 (-10+3)"],
			["val"=> "ten_-6"  , "name"=> "-6 (-10+4)"],
			["val"=> "ten_-5"  , "name"=> "-5 (-10+5)"],
			["val"=> "ten_-4"  , "name"=> "-4 (-10+6)"],
			["val"=> "ten_-3"  , "name"=> "-3 (-10+7)"],
			["val"=> "ten_-2"  , "name"=> "-2 (-10+8)"],
			["val"=> "ten_-1"  , "name"=> "-1 (-10+9)"]
		],
		'ext'=>[

			['val'=>'multi', 'name'=>"умножение"],
			['val'=>'division', 'name'=>"деление"],
		]
	];
	function __construct() {
		$this->helper = new cHelper();
		$this->user = new cUsers();
	}
	public function add($arData){
		return [
			'id_task'=>$this->helper->insert($this->tb, $arData)
		];
	}
	public function edit($id_task, $arData){

		$sql="";
		$arSQL =["UPDATE", $this->tb, "SET"];

		if ($id_task!=''){

			$arSet = [];
			foreach ($arData as $field => $value){
				$arSet[] = "$field=:$field";
			}
			$arSQL[] = implode(', ', $arSet);
			$arSQL[] = "WHERE";
			$arSQL[] = "id_task=:id_task";
			$arData['id_task']= $id_task;
			$sql = implode(' ', $arSQL);
//			echo $sql;
			DB::run($sql, $arData);
			return ['id_task'=>$id_task];
		}

	}
	public function delete($id_task){}
	public function getList($arFilter){

		$arSQL = ["SELECT * FROM"];
		$arSQL[] = $this->tb;

		if(!empty($arFilter)){
			$arSQL[] = "WHERE";
			$arWhere = [];
			foreach ($arFilter as $field=>$val){
				$arWhere[] = "$field=:$field";
			}
			$arSQL[] = implode(' AND ', $arWhere);
		}
		return DB::run(implode(" ", $arSQL), $arFilter)->fetchAll();

	}
	public function getByID($id_task){


		$sql = "SELECT * FROM $this->tb WHERE id_task=:id_task";

		$arTask =  DB::run($sql, ['id_task'=>$id_task])->fetch();
		$arTask['rules_five'] = (!empty($arTask['rules_five'])?explode(',', $arTask['rules_five']):[]);
		$arTask['rules_ten'] = (!empty($arTask['rules_ten'])?explode(',', $arTask['rules_ten']):[]);
		$arTask['rules_ext'] = (!empty($arTask['rules_ext'])?explode(',', $arTask['rules_ext']):[]);
		return $arTask;

	}
	public function add_task_result($data){

		$arTest = $data['test'];
		unset($data['test']);
		$arReturn  = [
			'id_task_user' =>$this->helper->insert($this->tb_task_user, $data)
		];
		foreach($arTest as $item){
			$item['id_task_user']=$arReturn['id_task_user'];
			$this->helper->insert($this->tb_task_user_answer, $item);
		}

	}
	public function get_task_result($id_task){
		$sql = "SELECT * FROM $this->tb_task_user WHERE id_task=:id_task";
		$results = DB::run($sql, ['id_task'=>$id_task])->fetchAll();
		$arUsers = [];
		foreach ($results as $result){
			if(!isset($arUsers[$result['id_user']])){
				$arUsers[$result['id_user']]  = $this->user->getByID($result['id_user']);
			}
		}
		return [
			'results' => $results,
			'users'=> $arUsers
		];

	}

}