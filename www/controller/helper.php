<?php
/**
 * Created by PhpStorm.
 * User: vvzakharov
 * Date: 30.09.2018
 * Time: 0:01
 */

Class cHelper{
	public function generateUId()
	{
		$idMask = "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx";

		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
		$result = '';
		for ($i = 0; $i < strlen($idMask); $i++) {
			if ($idMask[$i] == 'x') {
				$result .= $characters[mt_rand(0, 34)];
			} else {
				$result .= "-";
			}
		}
		return $result;
	}
	public function delete($tb, $arData){
		$sql = "" ;
		$arSQL =[];
		$arSQL[] = "DELETE FROM";
		$arSQL[] = $tb;
		if(!empty($arData)){
			$arSQL[] = "WHERE";
			$arWhere = [];
			foreach ($arData as $field => $value ){
				$arWhere[] = "$field = :$field";
			}
			$arSQL[] = implode(" ", $arWhere);
			$sql = implode(" ",$arSQL);
			return DB::run($sql, $arData);
		}
		return false;


	}
	public function insert($tb, $arData){
		$fields=array_keys($arData); // here you have to trust your field names!
		$values=array_values($arData);
		$fieldlist=implode(',',$fields);
		$qs=str_repeat("?,",count($fields)-1);
		$sql="insert into $tb ($fieldlist) values(${qs}?)";
		$stmt = DB::prepare($sql);
		$stmt->execute($values);
		return DB::lastInsertId();
	}

	public function getList($tb, $arData = []){
		$fields  = '*';
		$where = '';
		$ar_where = [];
		if(isset($arData['fields'])){
			$fields = implode(',',$arData['fields']);
		}
		if(isset($arData['where'])){
			if(!empty($arData['where'])){
				$ar_where = $arData['where'];
				$ar_fields = [];
				foreach($arData['where'] as $code => $val){
					$ar_fields[]="$code = :$code";
				}
				$where = "WHERE ".implode(" AND ", $ar_fields);
			}
		}
		$ar_sql = [
			"SELECT", $fields, "FROM", $tb, $where
		];
		$sql = implode(" ", $ar_sql);


//		echo $sql;
		return DB::run($sql, $ar_where )->fetchAll();
	}

	public function get_fields_key($arr){
		return array_keys($arr);
	}
	public function get_fields_val($arr){
		return array_values($arr);
	}
	public function get_fields_list($arr){
		$fields=array_keys($arr);
		return implode(',',$fields);
	}
	public function s_date_rus($s_date, $show_time=false){
		$o_date = DateTime::createFromFormat('Y-m-d H:i:s', $s_date);
		$rule = 'd.m.Y'.($show_time?' H:i':'');
		return $o_date->format($rule);

	}
	public function fnum($i, $count_fl=2){
		return '<span class="nowrap">'.($i>0?number_format($i, $count_fl, ',', ' '):'').'</span>';
	}


}
