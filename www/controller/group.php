<?php
/**
 * Created by PhpStorm.
 * User: vvzakharov
 * Date: 19.09.2018
 * Time: 21:30
 */
class cGroup {
	private $tb= 't_groups';
	private $tb_users= 't_users';
	private $t_groups_users ='t_groups_users';
	private $helper;
	function __construct() {
		$this->helper = new cHelper();
	}
	public function getShortList(){
		$arList = $this->helper->getList($this->tb);
		$arReturn = [];
		foreach($arList as $item){
			$arReturn[$item['id_group']] = $item['name'];
		}
		return $arReturn;

	}
	public function getList($where=[]){

		$arData = [];

		if(!empty($where))$arData['where']=$where;
		$arList = $this->helper->getList($this->tb, $arData);
		$arReturn = [];
		foreach($arList as $item){
			$arReturn[$item['id_group']] = $item;
		}
		return $arReturn;
	}
	public function getByID($id){
		$row = DB::run("SELECT * FROM $this->tb WHERE id_group = :id_group", ['id_group'=>$id])->fetch();



		if(!$row){
			return array('error'=>'Группа не найдена');
		}else{

		}
		return $row;
	}
	public function add($group=array()){

//		$sql = "INSERT INTO t_users (`email`, `f_name`, `l_name`, `s_name`, `type`) VALUES (:email, :f_name, :l_name, :s_name, :type);";
$id_group = 		$this->helper->insert($this->tb, $group);
return $this->getByID($id_group);
		//INSERT INTO t_users (`email`, `name`, `type`, `id_project`) VALUES ('test@test.ru', 'test', '2', '1');

	}
	public function edit($id_group, $group=array()){
		$sql = "UPDATE $this->tb SET ";
		$arSet = [];
		foreach($group as $field=>$value){
			$arSet[]="$field = :$field";
		}
		$sql.=implode(', ', $arSet)." WHERE id_group = :id_group";
		$group['id_group'] = $id_group;
		DB::run($sql, $group);
		return $group;
	}
	public function delete($id_group=''){

		if($id_group>0){
			$sql = "DELETE FROM $this->tb WHERE id_group = :id_group";
			$group = ['id_group'=> $id_group];
			DB::run($sql, $group);
			return ['code'=>200];
		}else{
			return ['code'=>500];
		}
	
		
	}
	public function getGroupByUser($id_user){
		$sql = "SELECT * FROM $this->t_groups_users  WHERE id_user=:id_user ";
		return DB::run($sql, ['id_user'=>$id_user])->fetch();
	}
}
