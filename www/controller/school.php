<?php
/**
 * Created by PhpStorm.
 * User: vvzakharov
 * Date: 19.09.2018
 * Time: 21:30
 */
class cSchool {
	private $tb= 't_schools';
	private $tb_users= 't_users';
	private $helper;
	function __construct() {
		$this->helper = new cHelper();
	}
	public function getShortList(){
		$arList = $this->helper->getList($this->tb);
		$arReturn = [];
		foreach($arList as $item){
			$arReturn[$item['id_school']] = $item['name'];
		}
		return $arReturn;

	}
	public function getList(){
		$arList = $this->helper->getList($this->tb);
		$arReturn = [];
		foreach($arList as $item){
			$arReturn[$item['id_school']] = $item;
		}
		return $arReturn;
	}
	public function getByID($id){
		$row = DB::run("SELECT * FROM $this->tb WHERE id_school = :id_school", ['id_school'=>$id])->fetch();

		if(!$row){
			return array('error'=>'Школа с ид '.$id.' не найдена');
		}else{
			$adm_sql = "SELECT * FROM $this->tb_users WHERE id_school=:id_school AND type = 2";
			$adm_row = DB::run($adm_sql, ['id_school'=>$id])->fetchAll();
			$row['admins'] = $adm_row;


			$teach_sql = "SELECT * FROM $this->tb_users WHERE id_school=:id_school AND type = 3";
			$teach_row = DB::run($teach_sql, ['id_school'=>$id])->fetchAll();



			$row['teachs'] = [];


			foreach($teach_row as $item){
				$row['teachs'][$item['id_user']] = $item;


			}
		}
		return $row;
	}
	public function add($school=array()){

//		$sql = "INSERT INTO t_users (`email`, `f_name`, `l_name`, `s_name`, `type`) VALUES (:email, :f_name, :l_name, :s_name, :type);";
		$id_school = 		$this->helper->insert($this->tb, $school);
		return $this->getByID($id_school);
		//INSERT INTO t_users (`email`, `name`, `type`, `id_project`) VALUES ('test@test.ru', 'test', '2', '1');

	}
	public function edit($id_school, $school=array()){
		$sql = "UPDATE $this->tb SET ";
		$arSet = [];
		foreach($school as $field=>$value){
			$arSet[]="$field = :$field";
		}
		$sql.=implode(', ', $arSet)." WHERE id_school = :id_school";
		$school['id_school'] = $id_school;
		DB::run($sql, $school);
		return $school;
	}
	public function delete($id_school=''){

		if($id_school>0){
			$sql = "DELETE FROM $this->tb WHERE id_school=:id_school";
			DB::run($sql, ['id_school' => $id_school]);
			return ['code'=>200];
		}else{
			return ['code'=>500];
		}
		
	}
}
