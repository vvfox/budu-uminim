<?php



Class cLessons{
	private $tb  = "t_lesson";
	private $tb_users = "t_lesson_users";
	private $tb_groups_user = "t_groups_users";
	private $helper;

	function __construct() {
		$this->helper = new cHelper();
		$this->user = new cUsers();
	}

	public function add_less($data){
		return $this->helper->insert($this->tb, $data);
	}
	public function edit_less($id_less, $data_lesson){
		$sql = "UPDATE $this->tb 
			SET date_lesson = :date_lesson, id_group=:id_group 
			WHERE id_lesson=:id_lesson";
		DB::run($sql, [
			'date_lesson' => $data_lesson['date_lesson'],
			'id_lesson' => $id_less,
			'id_group'=> $data_lesson['id_group']
		]);
		return true;
	}
	public function delete_less($id_less){

	}
	public function get_list_less_user($id_group){

		$sql = "SELECT * FROM $this->tb_users WHERE id_group=:id_group";


		$rets = DB::run($sql, ['id_group'=>$id_group])->fetchAll();

		$arReturn =[];

		foreach ($rets as $ret){
			$arReturn[] = $ret['id_user'].'_'.$ret['id_lesson'];
		}
		return $arReturn;
	}

	public function up_lesson_user($id_user, $id_lesson, $id_group, $val){
		if($val){
			$this->helper->insert($this->tb_users, [
				'id_user'=>$id_user,
				'id_lesson'=>$id_lesson,
				'id_group'=>$id_group
			]);
		}else{
			$sql = "DELETE FROM $this->tb_users WHERE id_user=:id_user AND id_lesson=:id_lesson";
			return DB::run($sql, [
				'id_user'=>$id_user,
				'id_lesson'=>$id_lesson
			]);
		}
	}
	public function get_list_less($id_group){
		$sql = "SELECT * FROM $this->tb WHERE id_group=:id_group ORDER BY date_lesson DESC";
		$rets = DB::run($sql, ['id_group'=>$id_group])->fetchAll();
		$arReturn = [];
		foreach ($rets as $ret){
			$dt = explode(' ', $ret['date_lesson']);
			$arReturn[$ret['id_lesson']] = explode('-',$dt[0]);
		}
		return $arReturn;
	}

}