<? $user->up_auth();?>
<html lang="ru">
<head>

    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/local/templates/.default/css/style.css">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <title><?=$title_page;?></title>
</head>
<body>

<? if(!empty($_ERRORS)){?>
    <div class="errors-message">
    <?foreach($_ERRORS as $ind => $error_line){?>
        <div><?=$error_line?></div>
    <?}?>
    </div>
<?}?>
<?if(!$user->is_auth()) {
    ?>

	<?
	include $_SERVER['DOCUMENT_ROOT'] . '/tmp/forms/auth_form.php';
	require_once $_SERVER['DOCUMENT_ROOT'] . '/' . CMS_PATH . '/footer.php';
	die();
}else{


?>

<article class="article">
<?}?>