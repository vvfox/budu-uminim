<?php
/**
 * Created by PhpStorm.
 * User: vvzakharov
 * Date: 28.08.2018
 * Time: 15:47
 */

session_start();
define('PRODUCT_MODE', false);
define('CMS_PATH', 'tmp');

global $_ERRORS;
$_ERRORS = array();

//define('CMS_PATH', 'bitrix');
function isValidEmail($email){
	return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
}


$arMenu = array(



	array( 'url'=>"/schools/", "name"=>"Школы", 'param'=>array(), 'chain_detect'=>true),
	array( 'url'=>"/groups/", "name"=>"Группы", 'param'=>array(), 'chain_detect'=>true),
	array( 'url'=>"/users/", "name"=>"Пользователи", 'param'=>array(), 'chain_detect'=>true),
	array( 'url'=>"/tasks/", "name"=>"Задания", 'param'=>array(), 'chain_detect'=>true),

//	array( 'url'=>"/reports/", "name"=>"Отчёты", 'param'=>array(), 'chain_detect'=>true),
//	array( 'url'=>"/orders/", "name"=>"Архив", 'param'=>array('filter'=>'archive'), 'chain_detect'=>true),
//	array( 'url'=>"/users/", "name"=>"Сотрудники", 'param'=>array(), 'chain_detect'=>true),

);




$arControllers = array(

	'helper',
	'users',
	'school',
	'group',
	'task',
	'lessons'
//	'parser_facture',
//	'project',
//	'firm',
//	'chains',
//	'orders'
);
require_once $_SERVER['DOCUMENT_ROOT']."/core/DB.php";

foreach ($arControllers as $control){
	require_once $_SERVER['DOCUMENT_ROOT'].'/controller/'.$control.'.php';
}
global $user;
$user = new cUsers();

global $helper;
$helper = new cHelper();

global $school;
$school = new cSchool();

global $group;
$group = new cGroup();

global $task;
$task = new cTask();


global $lesson;
$lesson = new cLessons();



//require_once $_SERVER['DOCUMENT_ROOT'].'/controller/parser_facture.php';
//include $_SERVER['DOCUMENT_ROOT'].'/controller/users.php';
//include $_SERVER['DOCUMENT_ROOT'].'/controller/chains.php';
//require_once $_SERVER['DOCUMENT_ROOT'].'/controller/firm.php';
global $arUser;
$arUser = array();
$arProject = array();
$arChains = array();

if($user->is_auth()) {
	$arUser = $user->getByID($user->getID());

	if(!isset($arUser['error'])){
//		if($arUser['id_schol']!=''){
//			$arProject = $project->getByID($arUser['id_schol']);
//
//			$getChains = $chains->getListByProject($arUser['id_schol']);
//			if(!isset($getChains['error'])){
//				$arChains = $getChains;
//			}else{
////				$_ERRORS[]= 'not found chains';
//			}
//
//		}else{
//			$_ERRORS[]='Не указана школа';
//		}
	}else{
		$_ERRORS[]=$arUser['error'];
	}

}
if(isset($_GET['logout'])){
	$user->logout();
}


global $action;
$action = 'preview';
$title_page ='Буду умным';