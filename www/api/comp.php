<?php
/**
 * Created by PhpStorm.
 * User: vvzakharov
 * Date: 26.09.2018
 * Time: 22:03
 */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
require_once $_SERVER['DOCUMENT_ROOT'].'/define/options.php';


$arReturn = array();
if(isset($_REQUEST['data']) && isset($_REQUEST['action'])){
	$action =$_REQUEST['action'];
	$data = $_REQUEST['data'];
	if($action=='get-firm'){


		if(isset($data['id_chain'])){
			$chains->edit_chain($data['id_chain'], $data['items']);
		}else{
			$chains->add_chain($data['items']);
		}


	}elseif($action=='get-comps'){

		$arReturn = $firm->getFirmClients($data['id_project']);

	}elseif ($action == 'get_comp'){
//		$arReturn['data'] = $data['id_comp'];
		$arReturn = $firm->getByID($data['id_comp']);
	}elseif($action=='save-firm'){
		$id_comp =$data['id_comp'];
		unset($data['id_comp']);

		$arFirmFND = $firm->getByID($id_comp);

		if(!isset($arFirmFND['error'])){
			$arReturn = $firm->edit($data, $id_comp);
		}else{
			$arReturn = $firm->add($data);
		}

	}elseif($action=="get-from-inn"){
		$inn = $data['inn'];

		if($inn !==''){
			$api_token = "9fd6629c728fbb09185a6619d32c479f34330a8a";
			$secrt_key = "31e8cbc2b376ed85f62b921f49b9122f5ad4139e";

			$url = "https://suggestions.dadata.ru/suggestions/api/4_1/rs/findById/party";
			$data = ["query"=>$inn];


			$headers = [
				"Content-Type: application/json",
				"Accept: application/json",
				"Authorization: Token ".$api_token
			];


			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			$results = curl_exec($ch);
			$arRes = json_decode($results, true);
			curl_close($ch);

			if(isset($arRes['suggestions'])){
				if(isset($arRes['suggestions'][0])) {
					$arDataFirm = $arRes['suggestions'][0]['data'];
					$man = $arDataFirm["management"];
					$man_name =$man['name'];
					$arManName = explode(' ', $man_name);
					$arReturn = [
						"inn" => $arDataFirm["inn"],
						"signer_f"=>$arManName[0],
						"signer_i"=>$arManName[1],
						"signer_o"=>$arManName[2],
						"full_name"=>$arDataFirm['name']['full_with_opf'],
						"name"=>$arDataFirm['name']['short_with_opf'],
						"kpp"=>$arDataFirm['kpp'],
						"ogrn"=>$arDataFirm['ogrn'],
						"okved"=>$arDataFirm['okved'],
						"oktmo"=>$arDataFirm['address']['data']['oktmo']

					];

				}
			}
		}else{
			$arReturn['error'] = 'not set inn';
		}
	}else{
		$arReturn['error'] = 'unknow action';
	}
}else{
	$arReturn['error'] = 'not set action';
}
echo json_encode($arReturn);
