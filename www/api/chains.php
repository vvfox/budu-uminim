<?php
/**
 * Created by PhpStorm.
 * User: vvzakharov
 * Date: 26.09.2018
 * Time: 20:32
 */error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
require_once $_SERVER['DOCUMENT_ROOT'].'/define/options.php';


$arReturn = array();
if(isset($_REQUEST['data']) && isset($_REQUEST['action'])){
	$action =$_REQUEST['action'];
	$data = $_REQUEST['data'];
	if($action=='save-chains'){
		$y = $data['y'];
		$q = $data['q'];
		$id_project = $data['id_project'];
		if(isset($data['id_chain'])){
			$arReturn= $chains->edit_chain($data['id_chain'], $data['items']);
		}else{
			$arReturn = $chains->add_chain($y, $q, $id_project, $data['items']);
		}
	}elseif($action=='update-correct'){
		$id_chain= $data['id_chain'];
		$id_comp= $data['id_comp'];
		$correct_num = $data['correct_num'];
		$arReturn = $chains->update_correct($id_chain, $id_comp, $correct_num);


	}elseif($action=="update-factures"){
		if(isset($data['items'])){
			if(isset($data['quartal']) && isset($data['year'])){
				$arDateStartEnd = $helper->get_dates_of_quarter(intval($data['quartal']), intval($data['year']), 'Y-m-d');
				$arReturn['items']  = array();
				$arReturn['data']  = $data;
				$arReturn['quartal']  = $arDateStartEnd;
				foreach($data['items'] as $item){
					$rnd_date = $helper->randomDate($arDateStartEnd['start'], $arDateStartEnd['end']);
					$f_date = date( 'Y-m-d H:m:s', $rnd_date);
					$arReturn['items'][$item]  = [
						'date'=>$helper->s_date_rus($f_date),
					];
					$arReturn['result'][$item] = $orders->update_f($item, ['date'=>$f_date]);
				}
			}elseif (isset($data['id_seller'])){
				$id_seller = $data['id_seller'];
				$arReturn['items']  = array();
				foreach($data['items'] as $item){
					$arReturn['items'][$item]  = [ 'id_seller'=>$id_seller, ];
					$arReturn['result'][$item] = $orders->update_f($item, ['id_seller'=>$id_seller]);
				}
			}elseif (isset($data['id_buyer'])){
				$id_buyer = $data['id_buyer'];
				$arReturn['items']  = array();
				foreach($data['items'] as $item){
					$arReturn['items'][$item]  = ['id_buyer'=>$id_buyer];
					$arReturn['result'][$item] = $orders->update_f($item, ['id_buyer'=>$id_buyer]);
				}
			}elseif (isset($data['is_delete'])){
				$is_delete = $data['is_delete'];
				$arReturn['items']  = array();
				foreach($data['items'] as $item){
					$arReturn['items'][$item]  = [ 'is_delete'=>$is_delete, ];
					$arReturn['result'][$item] = $orders->update_f($item, ['is_delete'=>$is_delete]);
				}
			}
		}else{
			$arReturn['error'] = 'not set items to update';
		}
	}elseif ($action=='gen-facture'){


		$arReturn = $chains->gen_facture($data['quartal'],$data['year'],$data['id_buyer'],$data['id_seller'],$data['id_chain'],$data['count_factures'],$data['sum_factures'],$data['wat_factures']);

//		quartal: quartal,
//		year: year,
//		id_buyer:id_buyer,
//		id_seller:id_seller,
//		id_chain: id_chain,
//		count_factures:count_factures,
//		sum_factures:sum_factures,
//		wat_factures:wat_factures,



	}

}
echo json_encode($arReturn);
