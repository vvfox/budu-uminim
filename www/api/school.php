<?php
/**
 * Created by PhpStorm.
 * User: vvzakharov
 * Date: 19.09.2018
 * Time: 22:29
 */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
require_once $_SERVER['DOCUMENT_ROOT'].'/define/options.php';
$entityBody = file_get_contents('php://input');
$entity = json_decode($entityBody,true);
$arReturn = array();
$arErrors = array();
$data = [];
if(isset($entity['data']) && isset($entity['action'])){
	$data = $entity['data'];


	if($entity['action']=='save'){
//	print_r($data);
		if(isset($data['admins'])){
			unset($data['admins']);
		}
		if(isset($data['teachs'])){
			unset($data['teachs']);
		}
		if(isset($data['id_school'])){
			$id_school = $data['id_school'];
			unset($data['id_school']);
			$arReturn = $school->edit($id_school, $data);
		}else{
			$arReturn = $school->add($data);
		}
	}elseif($entity['action']=='get'){
		$arReturn = [
			'school'=>$school->getByID($data['id_school']),
			'groups'=>$group->getList(['id_school'=>$data['id_school']])
		];
	}elseif($entity['action']=='delete-school'){
		$id_school = $data['id_school'];
		$arReturn = $school->delete($id_school);

	}elseif($entity['action']=='list'){
		$arReturn = [
			'items'=>$school->getList(),
		];
	}else{
		$arErrors[] = "not set action";
		$arReturn["error"] = "not set action";
	}
}else{
	$arErrors[] = "not set action and data";
	$arReturn["error"] = "not set action and data";
	$arReturn["data"] = $entity;
}
if(!empty($arErrors)){
	http_response_code(201);
//	header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
}
echo json_encode($arReturn);

