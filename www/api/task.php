<?php
/**
 * Created by PhpStorm.
 * User: vvzakharov
 * Date: 26/11/2018
 * Time: 08:35
 */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
require_once $_SERVER['DOCUMENT_ROOT'].'/define/options.php';
$entityBody = file_get_contents('php://input');
$entity = json_decode($entityBody,true);
$arReturn = array();
$arErrors = array();
$data = [];
if(isset($entity['data']) && isset($entity['action'])){
	$data = $entity['data'];
	if($entity['action']=='save'){

		//print_r($data);
		if(isset($data['id_task'])){
			$id_task = $data['id_task'];
			unset($data['id_task']);

//			print_r($data);
			$arReturn = $task->edit($id_task, $data);
		}else{
			$arReturn = $task->add($data);
		}
	}elseif($entity['action']=='get'){
		$id_task = $data['id_task'];
		$arReturn = [
			'task'=>$task->getByID($id_task),
			'rules'=>$task->rules
		];
	}elseif($entity['action']=='get-full'){
		$id_task = $data['id_task'];
		$arReturn = [
			'task'=>$task->getByID($id_task),
			'answers'=>$task->get_task_result($id_task),
			'rules'=>$task->rules
		];


	}elseif($entity['action']=='get-by-group'){
		$arReturn['tasks']=$task->getList(['id_group'=>$data['id_group']]);
	}elseif($entity['action']=='add-task-user'){
		$arReturn = $task->add_task_result($data);
	}elseif($entity['action']=='get-rules'){
		$arReturn = ['rules'=>$task->rules];
	}else{
		$arErrors[] = "not set action";
		$arReturn["error"] = "not set action";
	}
}else{
	$arErrors[] = "not set action and data";
	$arReturn["error"] = "not set action and data";
	$arReturn["data"] = $entity;
}
if(!empty($arErrors)){
	http_response_code(201);
//	header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
}
echo json_encode($arReturn);