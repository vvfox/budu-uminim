<?php
/**
 * Created by PhpStorm.
 * User: vvzakharov
 * Date: 19.09.2018
 * Time: 22:29
 */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
require_once $_SERVER['DOCUMENT_ROOT'].'/define/options.php';
$entityBody = file_get_contents('php://input');
$entity = json_decode($entityBody,true);
$arReturn = array();
$arErrors = array();
$data = [];


if(isset($entity['data']) && isset($entity['action'])){
	$data = $entity['data'];
	if($entity['action']=='auth') {
		$email    = $entity['data']['email'];
		$arReturn = $user->get_token( $email );
	}elseif($entity['action']=='get-user'){
		if(isset($_SESSION['user'])){
			$arReturn = $user->getByID($_SESSION['user']);
		}else{
			$arReturn['error'] = 'not auth';
		}
	}elseif($entity['action']=='save'){
		$id_group = "";

		if(isset($data['id_group'])){

			$id_group= $data['id_group'];
			unset($data['id_group']);
		}

		if(!isset($data['active'])) $data['active'] = 0;
		if($data['active']=="")$data['active']=0;
//		echo "data_active".$data['active'];
		if(isset($data['id_user'])){
			$id_user = $data['id_user'];
			unset($data['id_user']);
			$arReturn = $user->edit($id_user, $data);
		}else{
			$arReturn = $user->add($data);
			$id_user  = $arReturn['id_user'];
		}
		if($id_group!==''){
			$user->setGroupUser($id_user, $id_group);
		}
	}elseif($entity['action']=='get-groups'){
		$arReturn = [
			'groups'=>$user->getGroups(),
			'schools'=>$school->getShortList()
		];
	}elseif($entity['action']=='get'){
		$arReturn = [
			'user'=>$user->getByID($data['id_user']),
			'groups'=>$user->getGroups(),
			'schools'=>$school->getShortList()
		];
	}elseif($entity['action']=='list'){
		$arReturn = [
			'items'=>$user->getList(),
			'groups'=>$user->getGroups(),
			'schools'=>$school->getShortList()
		];
	}else{
		$arErrors[] = "not set action";
		$arReturn["error"] = "not set action";
	}
}else{
	$arErrors[] = "not set action and data";
	$arReturn["error"] = "not set action and data";
	$arReturn["data"] = $entity;
}
if(!empty($arErrors)){
	http_response_code(201);
//	header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
}
echo json_encode($arReturn);

