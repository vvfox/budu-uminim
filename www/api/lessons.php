<?php
/**
 * Created by PhpStorm.
 * User: vvzakharov
 * Date: 01/12/2018
 * Time: 18:54
 */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
require_once $_SERVER['DOCUMENT_ROOT'].'/define/options.php';
$entityBody = file_get_contents('php://input');
$entity = json_decode($entityBody,true);
$arReturn = array();
$arErrors = array();
$data = [];
if(isset($entity['data']) && isset($entity['action'])){
	$data = $entity['data'];
	if($entity['action']=='add-lesson'){
		$date_lesson =date ("Y-m-d", strtotime($data['date_lesson']));


		$arReturn = [
			'id_lesson'=>$lesson->add_less([
				'date_lesson' => $date_lesson,
				'id_group'=>$data['id_group']
			]),
			'request'=>$entity
		];
	}
	elseif($entity['action']=='edit-lesson'){
		$id_lesson = $data['id_lesson'];
		$date_lesson = date("H:i:s", strtotime($data['date_lesson']));

		$arReturn = $lesson->edit_less($id_lesson, [
			'date_lesson' => $date_lesson,
			'id_group'=>$data['id_group']
		]);
	}
	elseif($entity['action']=='get-lesson'){
//		echo 'get-lesson';
		$id_group = $data['id_group'];
		$arReturn = ['lessons'=>$lesson->get_list_less($id_group)];

	}elseif($entity['action']=='edit-user-lesson'){

		$arReturn = ['return'=>$lesson->up_lesson_user($data['id_user'], $data['id_lesson'], $data['id_group'],$data['val'])];


	}elseif($entity['action']=='get-user-lesson'){
		echo 'get-user-lesson';
	}else{
		echo $entity['action'];
	}
}
echo json_encode($arReturn);