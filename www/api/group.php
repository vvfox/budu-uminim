<?php
/**
 * Created by PhpStorm.
 * User: vvzakharov
 * Date: 19.09.2018
 * Time: 22:29
 */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
require_once $_SERVER['DOCUMENT_ROOT'].'/define/options.php';
$entityBody = file_get_contents('php://input');
$entity = json_decode($entityBody,true);
$arReturn = array();
$arErrors = array();
$data = [];
if(isset($entity['data']) && isset($entity['action'])){
	$data = $entity['data'];
	if($entity['action']=='save'){
		if(isset($data['id_group'])){
			$id_group = $data['id_group'];
			unset($data['id_group']);
			$arReturn = $group->edit($id_group, $data);
		}else{
			$arReturn = $group->add($data);
		}
	}elseif($entity['action']=='delete-group'){
		if(isset($data['id_group'])){
			$id_group = $data['id_group'];
			$arReturn = $group->delete($id_group);
		}else{
			$arReturn = ['code'=>505];
		}

	}elseif($entity['action']=='get-group-by-user'){
		$arReturn = ['group'=>$group->getGroupByUser($data['id_user'])];
	}elseif($entity['action']=='get'){
		$arReturn = ['group'=>$group->getByID($data['id_group'])];
		if(isset($arReturn['group']['id_school'])){
			$arReturn['school'] =$school->getByID($arReturn['group']['id_school']);

			$arUsersRet = $user->getListBySchoolType($arReturn['group']['id_school'], 3);
			$arReturn['users']=[];
			foreach ($arUsersRet as $arUser){
				$arReturn['users'][$arUser['id_user']] = $arUser['f_name'].' '.$arUser['l_name'];
			}

			$arUsersRet = $user->getListByGroupType($data['id_group'], 4);
			$arReturn['childs']=[];
			foreach ($arUsersRet as $arUser){
				$arReturn['childs'][$arUser['id_user']] = [
					'name'=>$arUser['f_name'].' '.$arUser['l_name'],
					'email'=>$arUser['email']

				];
			}
			$arReturn['tasks']=$task->getList(['id_group'=>$data['id_group']]);


		}
	}elseif($entity['action']=='get-full'){
		$arReturn = ['group'=>$group->getByID($data['id_group'])];
		if(isset($arReturn['group']['id_school'])){
			$arReturn['school'] =$school->getByID($arReturn['group']['id_school']);

			$arUsersRet = $user->getListBySchoolType($arReturn['group']['id_school'], 3);
			$arReturn['users']=[];
			foreach ($arUsersRet as $arUser){
				$arReturn['users'][$arUser['id_user']] = $arUser['f_name'].' '.$arUser['l_name'];
			}

			$arUsersRet = $user->getListByGroupType($data['id_group'], 4);
			$arReturn['childs']=[];
			foreach ($arUsersRet as $arUser){
				$arReturn['childs'][$arUser['id_user']] = [
					'name'=>$arUser['f_name'].' '.$arUser['l_name'],
					'email'=>$arUser['email']

				];
			}
			$arReturn['lessons_user']=$lesson->get_list_less_user($data['id_group']);
			$arReturn['lessons']=$lesson->get_list_less($data['id_group']);
			$arReturn['tasks']=$task->getList(['id_group'=>$data['id_group']]);
		}
	}elseif($entity['action']=='list-teacher'){

		$arReturn = [
			'items'=>$group->getList(['id_teacher'=>$data['id_teacher']])
		];

	}elseif($entity['action']=='list'){
		$arReturn = [
			'items'=>$group->getList(),
		];
	}elseif($entity['action']=='get-school-type-users'){

		//print_r($data);
		$arUsersRet = $user->getListBySchoolType($data['id_school'], $data['type']);
		$arReturn = ['users'=>[]];

		foreach ($arUsersRet as $arUser){
			$arReturn['users'][$arUser['id_user']] = $arUser['f_name'].' '.$arUser['l_name'];
		}
	}else{
		$arErrors[] = "not set action";
		$arReturn["error"] = "not set action";
	}
}else{
	$arErrors[] = "not set action and data";
	$arReturn["error"] = "not set action and data";
	$arReturn["data"] = $entity;
}
if(!empty($arErrors)){
	http_response_code(201);
//	header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
}
echo json_encode($arReturn);

