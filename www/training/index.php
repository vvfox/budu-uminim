<?
$arTask = array(
	"name"           => "Настроить",
	"count_numbers"  => 2,
	"count_question" => 10,
	"time_pause"     => 1.00,
	"len_numbers"    => 1,
	"rules_five"     => Array( "five_-4", "five_-3" ),
	"rules_ten"      => Array( "ten_-9" ),
	'rules_ext'      => array()
);
$date = new DateTime();
// echo $date->getTimestamp();
$arTaskTen = [];
$arTaskFive = [];
include __DIR__ . '/../define/options.php';
$taskRules = $task->rules;
if ( isset( $_GET['task'] ) ) {
	$arGetTask = $task->getByID( $_GET['task'] );

	$idUser = $user->getID();

	if ( isset( $arGetTask['id_task'] ) ) {
		$arTask = $arGetTask;
		if ( ! empty( $arTask['rules_five'] ) ) {
			foreach ( $arTask['rules_five'] as $code ) {
				$arTaskFive[ $code ] = $code;
			}
		}
		if ( ! empty( $arTask['rules_ten'] ) ) {
			foreach ( $arTask['rules_ten'] as $code ) {
				$arTaskTen[ $code ] = $code;
			}
		}
		if ( ! empty( $arTask['rules_ext'] ) ) {
			foreach ( $arTask['rules_ext'] as $code ) {
				$arTaskTen[ $code ] = $code;
			}
		}
	}
}
?><!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Генератор примеров для ментальной арифметике</title>
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,latin-ext"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,600;1,800;1,900&display=swap"
          rel="stylesheet">
    <link href="lib/css/fastselect.min.css" rel="stylesheet" type="text/css">
    <link href="lib/css/rangeslider.css" rel="stylesheet" type="text/css">
    <link href="../local/templates/.default/css/style.css?v=1.3" rel="stylesheet" type="text/css">
    <link href="css/select2.min.css" rel="stylesheet"/>

    <link href="css/app.css?v=1.2" rel="stylesheet" type="text/css">
    <link href="css/new.test.css?v=1.2" rel="stylesheet"/>

</head>
<body>
<div class="content">
    <div class="config">
        <div class="header-train">
            <div class="wrapper">
                <div class="clearfix ">
                    <h1 class="h1 logo">БудУмныМ.ру</h1>
                    <div class="desc">тренажер навыков ментальной арифметики</div>
                </div>
            </div>
        </div>
        <div class="conteiner">
            <div class="wrapper">
                <div class="colarea generator">
                    <div class=" col-pdg">
                        <h2 class="h2"><?= $arTask['name'] ?></h2>
                        <form action="" class="form" <?= ( isset( $arTask['id_task'] ) ? 'style=""' : '' ); ?>>
							<? if ( $idUser ) { ?>
                                <input type="hidden" id="id_user" value="<?= $idUser ?>">
							<? } ?>
							<? if ( isset( $arTask['id_task'] ) ) { ?>
                                <input type="hidden" id="id_task" value="<?= $arTask['id_task'] ?>">
							<? } ?>
                            <div class="f-row">
                                <div class="colarea">
                                    <div class="col2">
                                        <label for="time_pause" class="lbl">Пауза при показе чисел</label>
                                        <div class="rng-inp">
                                            <input type="range" id="time_pause" min=".1" max="10" step=".1"
                                                   value="<?= $arTask['time_pause'] ?>" data-rangeslider>
                                        </div>
                                    </div>
                                    <div class="col2">
                                        <label for="count_question" class="lbl">Кол-во заданий</label>
                                        <div class="rng-inp">
                                            <input type="range" id="count_question" min="1" max="40" step="1"
                                                   value="<?= $arTask['count_question'] ?>" data-rangeslider>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=" btn-control btn-control-error t-center" id="btn-control-error">
                            </div>
                            <div class="tabs">
                                <input type="radio" value="plusminus" name="tab_input" id="tab_input_plusminus">
                                <label for="tab_input_plusminus" class="tab">Сложение/вычитание</label>
                                <input type="radio" value="multi" name="tab_input" id="tab_input_multi">
                                <label for="tab_input_multi" class="tab">Умножение</label>
                                <input type="radio" value="division" name="tab_input" id="tab_input_division">
                                <label for="tab_input_division" class="tab">Деление</label>
                            </div>
                            <div class="tabs-content" id="tabs-content__multi">
                                <div class="f-row">
                                    <div class="colarea">
                                        <div class="col2">
                                            <label class="lbl">Разрядность первого множителя:</label>
                                            <div>
                                                <input name="m_len_first" class="m_len_first" type="radio" value="1"
                                                       id="m_len_first_1"><label for="m_len_first_1">1</label>
                                                <input name="m_len_first" class="m_len_first" type="radio" value="2"
                                                       id="m_len_first_2"><label for="m_len_first_2">2</label>
                                                <input name="m_len_first" class="m_len_first" type="radio" value="3"
                                                       id="m_len_first_3"><label for="m_len_first_3">3</label>
                                            </div>
                                        </div>
                                        <div class="col2">
                                            <label class="lbl">Разрядность второго множителя:</label>
                                            <div>
                                                <input name="m_len_second" class="m_len_second" id="m_len_second_1"
                                                       value="1" type="radio"><label for="m_len_second_1">1</label>
                                                <input name="m_len_second" class="m_len_second" id="m_len_second_2"
                                                       value="2" type="radio"><label for="m_len_second_2">2</label>
                                                <input name="m_len_second" class="m_len_second" id="m_len_second_3"
                                                       value="3" type="radio"><label for="m_len_second_3">3</label>
                                            </div>
                                        </div>
                                        <div class="col2">
                                            <label class="lbl">Знаков после запятой первого множителя:</label>
                                            <div>
                                                <input name="m_digits_first" class="m_digits_first"
                                                       id="m_digits_first_0" value="0" type="radio"><label
                                                        for="m_digits_first_0">0</label>
                                                <input name="m_digits_first" class="m_digits_first"
                                                       id="m_digits_first_1" value="1" type="radio"><label
                                                        for="m_digits_first_1">1</label>
                                                <input name="m_digits_first" class="m_digits_first"
                                                       id="m_digits_first_2" value="2" type="radio"><label
                                                        for="m_digits_first_2">2</label>
                                                <input name="m_digits_first" class="m_digits_first"
                                                       id="m_digits_first_3" value="3" type="radio"><label
                                                        for="m_digits_first_3">3</label>
                                            </div>
                                        </div>
                                        <div class="col2">
                                            <label class="lbl">Знаков после запятой второго множителя:</label>
                                            <div>
                                                <input name="m_digits_second" class="m_digits_second"
                                                       id="m_digits_second_0" value="0" type="radio"><label
                                                        for="m_digits_second_0">0</label>
                                                <input name="m_digits_second" class="m_digits_second"
                                                       id="m_digits_second_1" value="1" type="radio"><label
                                                        for="m_digits_second_1">1</label>
                                                <input name="m_digits_second" class="m_digits_second"
                                                       id="m_digits_second_2" value="2" type="radio"><label
                                                        for="m_digits_second_2">2</label>
                                                <input name="m_digits_second" class="m_digits_second"
                                                       id="m_digits_second_3" value="3" type="radio"><label
                                                        for="m_digits_second_3">3</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs-content" id="tabs-content__division">

                                <div class="f-row">
                                    <div class="colarea">

                                        <div class="col2">
                                            <label class="lbl">Разрядность делимого:</label>
                                            <div>
                                                <input name="d_len_first" class="d_len_first" type="radio" value="1"
                                                       id="d_len_first_1"><label for="d_len_first_1">1</label>
                                                <input name="d_len_first" class="d_len_first" type="radio" value="2"
                                                       id="d_len_first_2"><label for="d_len_first_2">2</label>
                                                <input name="d_len_first" class="d_len_first" type="radio" value="3"
                                                       id="d_len_first_3"><label for="d_len_first_3">3</label>
                                            </div>
                                        </div>
                                        <div class="col2">
                                            <label class="lbl">Разрядность делителя:</label>
                                            <div>
                                                <input name="d_len_second" class="d_len_second" id="d_len_second_1"
                                                       value="1" type="radio"><label for="d_len_second_1">1</label>
                                                <input name="d_len_second" class="d_len_second" id="d_len_second_2"
                                                       value="2" type="radio"><label for="d_len_second_2">2</label>
                                                <input name="d_len_second" class="d_len_second" id="d_len_second_3"
                                                       value="3" type="radio"><label for="d_len_second_3">3</label>
                                            </div>
                                        </div>
                                        <div class="col2">

                                            <label class="lbl">Знаков после запятой делимого:</label>
                                            <div>
                                                <input name="d_digits_first" class="d_digits_first"
                                                       id="d_digits_first_0" value="0" type="radio"><label
                                                        for="d_digits_first_0">0</label>
                                                <input name="d_digits_first" class="d_digits_first"
                                                       id="d_digits_first_1" value="1" type="radio"><label
                                                        for="d_digits_first_1">1</label>
                                                <input name="d_digits_first" class="d_digits_first"
                                                       id="d_digits_first_2" value="2" type="radio"><label
                                                        for="d_digits_first_2">2</label>
                                                <input name="d_digits_first" class="d_digits_first"
                                                       id="d_digits_first_3" value="3" type="radio"><label
                                                        for="d_digits_first_3">3</label>

                                            </div>
                                        </div>
                                        <div class="col2">

                                            <label class="lbl">Знаков после запятой делителя:</label>
                                            <div>
                                                <input name="d_digits_second" class="d_digits_second"
                                                       id="d_digits_second_0" value="0" type="radio"><label
                                                        for="d_digits_second_0">0</label>
                                                <input name="d_digits_second" class="d_digits_second"
                                                       id="d_digits_second_1" value="1" type="radio"><label
                                                        for="d_digits_second_1">1</label>
                                                <input name="d_digits_second" class="d_digits_second"
                                                       id="d_digits_second_2" value="2" type="radio"><label
                                                        for="d_digits_second_2">2</label>
                                                <input name="d_digits_second" class="d_digits_second"
                                                       id="d_digits_second_3" value="3" type="radio"><label
                                                        for="d_digits_second_3">3</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tabs-content" id="tabs-content__plusminus">

                                <div class="f-row">
                                    <div class="colarea">

                                        <div class="col2">
                                            <label for="count_numbers" class="lbl">Кол-во чисел в примере</label>
                                            <div class="rng-inp">
                                                <input type="range" id="count_numbers" min="2" max="25" step="1"
                                                       value="<?= $arTask['count_numbers'] ?>" data-rangeslider>
                                            </div>
                                        </div>
                                        <div class="col2">
                                            <label for="len_numbers" class="lbl">Кол-во разрядов</label>
                                            <div class="rng-inp">
                                                <input type="range" id="len_numbers" min="1" max="6" step="1"
                                                       value="<?= $arTask['len_numbers'] ?>" data-rangeslider>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="f-row">
                                    <label class="fieldCaption">Законы на 5:</label>
                                    <div class="select-all-panel">
                                        <span class="select-all" data-rule="five">выбрать все</span>
                                    </div>
                                    <select class="Select2" id="five" multiple="multiple" placeholder="Выберите закон">
										<? foreach ( $taskRules['five'] as $opt ) { ?>
                                            <option <?= ( isset( $arTaskFive[ $opt['val'] ] ) ? 'selected' : '' ) ?>
                                            value="<?= str_replace( 'five_', '', $opt['val'] ) ?>"><?= $opt['name'] ?></option><? } ?>
                                    </select>
                                </div>
                                <div class="f-row">
                                    <label class="fieldCaption">Законы на 10:</label>
                                    <div class="select-all-panel">
                                        <span class="select-all" data-rule="ten">выбрать все</span>
                                    </div>
                                    <select class="Select2" id="ten" multiple="multiple" placeholder="Выберите закон">
                                        <!--<option value="10" SELECTED>Любой</option>-->
										<? foreach ( $taskRules['ten'] as $opt ) { ?>
                                            <option <?= ( isset( $arTaskTen[ $opt['val'] ] ) ? 'selected' : '' ) ?>
                                            value="<?= str_replace( 'ten_', '', $opt['val'] ) ?>"><?= $opt['name'] ?></option><? } ?>
                                    </select>
                                </div>
                            </div>

                        </form>
                        <div class=" btn-control t-center" id="btn-control-start">
                            <a href="#" class="btn btn-start">Запустить&nbsp;тренажер</a>
                        </div>
                    </div>

                    <!--                -->
                    <div class="col-pdg list-result__simples" style="">
                        <h2 class="h2">Примеры</h2>
                        <div class="list_question">
                        </div>
                        <div class="f-row">
                            <button id="reset-numbers" class="btn">Сгенерировать ещё</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="b-test">
        <div class="b-timer b-frame">
            <div class="b-frame-center-content">
                Старт через <span class="count-second"></span>&nbsp;сек.
            </div>
        </div>
        <div class="b-quest b-frame" id="b-quest">
            <div class="b-frame-center-content">
                <div class="big-num" id="quest-num"></div>
            </div>
        </div>
        <div class="b-answer b-frame">
            <div class="b-frame-center-content">
                <div class="h1">Введите ответ</div>
                <input type="text" id="answer">
                <div class="btn-control"><a href="#" class="btn btn-send-answer">Ответить</a></div>
            </div>
            <div class="panel-control">
                <a href="#" id="test-stop" class="btn">Стоп</a>
            </div>
        </div>
        <div class="b-answer-valid b-frame">
            <div class="b-frame-center-content">
                <div class="result-answer"></div>
                <div class="result-answer-value"></div>
                <div class="result-answer-image"></div>
            </div>
        </div>
        <div class="b-result b-frame">

            <div class="h1">Тест завершен</div>
            <div class="b-result-test">
                <div class="b-count_samples">Всего примеров: <span class="count_samples"></span></div>
                <div class="b-count_corrects">Правильно: <span class="count_corrects"></span></div>
                <div class="b-count_error">Ошибок: <span class="count_error"></span></div>
                <div class="result-percent"></div>
            </div>
            <a href="#" class="btn btn-repeat">Повторить</a>
            <a href="#" class="btn btn-change-config">Сначала</a>
        </div>

    </div>

</div>

<style>
.list-result__simples{display:none;} 

</style>
<script src="lib/js/jquery.min.js"></script>
<script src="lib/js/fastselect.standalone.min.js"></script>


<script src="js/select2.min.js"></script>


<script src="lib/js/rangeslider.min.js"></script>
<script src="lib/axios.min.js"></script>
<script src="js/sampler.js?v=<?= $date->getTimestamp(); ?>"></script>
<script>
    (function samplerPage(window, $) {
        oSampler.init();
        $('#reset-numbers').click(function () {
            oSampler.exicute();
            return false;
        });
        $('.btn-send-answer').click(function () {
            oSampler.set_answer();
            return false;
        });
        $('.btn-start').click(function () {
            oSampler.start_test();
            return false;
        });
        $('.btn-repeat').click(function () {
            oSampler.exicute();
            oSampler.start_test();
            return false;
        });
    }(window, jQuery));
</script>
</body>
</html>