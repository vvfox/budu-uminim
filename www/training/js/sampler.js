String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};
var only_number = function (input) {
    var value = input.value;
    var re = /[^0-9\-]/gi;
    if (re.test(value)) {
        value = value.replace(re, '');
        input.value = value;
    }
};

var oSampler = {
    classes: {
        active: 'active'
    },
    config:{
        time_pause: 1,
        time_answer: 7,
        count_question: 0,
        count_numbers: 0,
        len_numbers: 0,
        rules:[]
    },
    test_inprogress: false,
    samples: [],
    min_lenth: 1,
    test:{
        time_to_start: 0,
        number_quest: 0,
        answers:{},
        timer_set_answer: 0,
        fn_timer_set_answer: false,
        play: false
    },
    rules:{
        'none':{
            0: [1,2,3,4,5,6,7,8,9],
            1: [1,2,3,5,6,7,8],
            2: [1,2,5,6,7, -1],
            3: [1,5,6,-1,-2, ],
            4: [5,-1,-2,-3,],
            5: [1,2,3,4, ],
            6: [1,2,3,-1,-5 ],
            7: [1,2,-1,-2, -5],
            8: [1,-1,-2,-3, -5],
            9: [-1,-2,-3,-4,-5]
        },
        'five_-4':{
            5:[-4], 6: [-4], 7: [-4], 8: [-4]
        },
        'five_-3':{
            5:[-3], 6:[-3], 7:[-3]
        },
        'five_-2':{
            5:[-2], 6:[-2]
        },
        'five_-1':{
            5:[-1]
        },
        'five_1':{
            4:[1]
        },
        'five_2':{
            3:[2], 4:[2]
        },
        'five_3':{
            2:[3], 3:[3], 4:[3]
        },
        'five_4':{
            1:[4], 2:[4], 3:[4], 4:[4]
        },
        'ten_9':{1:[9],2:[9],3:[9],4:[9],5:[9],6:[9],7:[9],8:[9],9:[9],},
        'ten_8':{2:[8],3:[8],4:[8],5:[8],6:[8],7:[8],8:[8],9:[8],},
        'ten_7':{3:[7],4:[7],5:[7],6:[7],7:[7],8:[7],9:[7],},
        'ten_6':{4:[6],5:[6],6:[6],7:[6],8:[6],9:[6],},
        'ten_5':{5:[5],6:[5],7:[5],8:[5],9:[5],},
        'ten_4':{6:[4],7:[4],8:[4],9:[4]},
        'ten_3':{7:[3],8:[3],9:[3]},
        'ten_2':{9:[2], 8:[2]},
        'ten_1':{9:[1]},

        // 'ten_-9':{18:[-9], 17:[-9],16:[-9], 15:[-9], 14:[-9], 13:[-9], 12:[-9], 11:[-9], 10:[-9]},
        // 'ten_-8':{17:[-8], 16:[-8], 15:[-8], 14:[-8], 13:[-8], 12:[-8], 11:[-8], 10:[-8]},
        // 'ten_-7':{16:[-7], 15:[-7], 14:[-7], 13:[-7], 12:[-7], 11:[-7], 10:[-7]},
        // 'ten_-6':{15:[-6], 14:[-6], 13:[-6], 12:[-6], 11:[-6], 10:[-6]},
        // 'ten_-5':{14:[-5], 13:[-5], 12:[-5], 11:[-5], 10:[-5]},
        // 'ten_-4':{13:[-4], 12:[-4], 11:[-4], 10:[-4]},
        // 'ten_-3':{12:[-3], 11:[-3], 10:[-3]},
        // 'ten_-2':{11:[-2], 10:[-2]},
        // 'ten_-1':{10:[-1]},



        'ten_-9':{
            18:[-9], 17:[-9],16:[-9], 15:[-9], 14:[-9], 13:[-9], 12:[-9], 11:[-9], 10:[-9],
            28:[-9], 27:[-9],26:[-9], 25:[-9], 24:[-9], 23:[-9], 22:[-9], 21:[-9], 20:[-9],
            38:[-9], 37:[-9],36:[-9], 35:[-9], 34:[-9], 33:[-9], 32:[-9], 31:[-9], 30:[-9],
            48:[-9], 47:[-9],46:[-9], 45:[-9], 44:[-9], 43:[-9], 42:[-9], 41:[-9], 40:[-9],
            58:[-9], 57:[-9],56:[-9], 55:[-9], 54:[-9], 53:[-9], 52:[-9], 51:[-9], 50:[-9],
            68:[-9], 67:[-9],66:[-9], 65:[-9], 64:[-9], 63:[-9], 62:[-9], 61:[-9], 60:[-9],
            78:[-9], 77:[-9],76:[-9], 75:[-9], 74:[-9], 73:[-9], 72:[-9], 71:[-9], 70:[-9],
            88:[-9], 87:[-9],86:[-9], 85:[-9], 84:[-9], 83:[-9], 82:[-9], 81:[-9], 80:[-9],
            98:[-9], 97:[-9],96:[-9], 95:[-9], 94:[-9], 93:[-9], 92:[-9], 91:[-9], 90:[-9]

        },
        'ten_-8':{
            17:[-8], 16:[-8], 15:[-8], 14:[-8], 13:[-8], 12:[-8], 11:[-8], 10:[-8],
            27:[-8], 26:[-8], 25:[-8], 24:[-8], 23:[-8], 22:[-8], 21:[-8], 20:[-8],
            37:[-8], 36:[-8], 35:[-8], 34:[-8], 33:[-8], 32:[-8], 31:[-8], 40:[-8],
            47:[-8], 46:[-8], 45:[-8], 44:[-8], 43:[-8], 42:[-8], 41:[-8], 40:[-8],
            57:[-8], 56:[-8], 55:[-8], 54:[-8], 53:[-8], 52:[-8], 51:[-8], 50:[-8],
            67:[-8], 66:[-8], 65:[-8], 64:[-8], 63:[-8], 62:[-8], 61:[-8], 60:[-8],
            77:[-8], 76:[-8], 75:[-8], 74:[-8], 73:[-8], 72:[-8], 71:[-8], 70:[-8],
            87:[-8], 86:[-8], 85:[-8], 84:[-8], 83:[-8], 82:[-8], 81:[-8], 80:[-8],
            97:[-8], 96:[-8], 95:[-8], 94:[-8], 93:[-8], 92:[-8], 91:[-8], 90:[-8]
        },
        'ten_-7':{
            16:[-7], 15:[-7], 14:[-7], 13:[-7], 12:[-7], 11:[-7], 10:[-7],
            26:[-7], 25:[-7], 24:[-7], 23:[-7], 22:[-7], 21:[-7], 20:[-7],
            36:[-7], 35:[-7], 34:[-7], 33:[-7], 32:[-7], 31:[-7], 30:[-7],
            46:[-7], 45:[-7], 44:[-7], 43:[-7], 42:[-7], 41:[-7], 40:[-7],
            56:[-7], 55:[-7], 54:[-7], 53:[-7], 52:[-7], 51:[-7], 50:[-7],
            66:[-7], 65:[-7], 64:[-7], 63:[-7], 62:[-7], 61:[-7], 60:[-7],
            76:[-7], 75:[-7], 74:[-7], 73:[-7], 72:[-7], 71:[-7], 70:[-7],
            86:[-7], 85:[-7], 84:[-7], 83:[-7], 82:[-7], 81:[-7], 80:[-7],
            96:[-7], 95:[-7], 94:[-7], 93:[-7], 92:[-7], 91:[-7], 90:[-7]

        },
        'ten_-6':{
            15:[-6], 14:[-6], 13:[-6], 12:[-6], 11:[-6], 10:[-6],
            25:[-6], 24:[-6], 23:[-6], 22:[-6], 21:[-6], 20:[-6],
            35:[-6], 34:[-6], 33:[-6], 32:[-6], 31:[-6], 30:[-6],
            45:[-6], 44:[-6], 43:[-6], 42:[-6], 41:[-6], 40:[-6],
            55:[-6], 54:[-6], 53:[-6], 52:[-6], 51:[-6], 50:[-6],
            65:[-6], 64:[-6], 63:[-6], 62:[-6], 61:[-6], 60:[-6],
            75:[-6], 74:[-6], 73:[-6], 72:[-6], 71:[-6], 70:[-6],
            85:[-6], 84:[-6], 83:[-6], 82:[-6], 81:[-6], 80:[-6],
            95:[-6], 94:[-6], 93:[-6], 92:[-6], 91:[-6], 90:[-6]

        },
        'ten_-5':{
            14:[-5], 13:[-5], 12:[-5], 11:[-5], 10:[-5],
            24:[-5], 23:[-5], 22:[-5], 21:[-5], 20:[-5],
            24:[-5], 23:[-5], 22:[-5], 21:[-5], 20:[-5],
            34:[-5], 33:[-5], 32:[-5], 31:[-5], 30:[-5],
            44:[-5], 43:[-5], 42:[-5], 41:[-5], 40:[-5],
            54:[-5], 53:[-5], 52:[-5], 51:[-5], 50:[-5],
            64:[-5], 63:[-5], 62:[-5], 61:[-5], 60:[-5],
            74:[-5], 73:[-5], 72:[-5], 71:[-5], 70:[-5],
            84:[-5], 83:[-5], 82:[-5], 81:[-5], 80:[-5],
            94:[-5], 93:[-5], 92:[-5], 91:[-5], 90:[-5]
        },
        'ten_-4':{
            13:[-4], 12:[-4], 11:[-4], 10:[-4],
            23:[-4], 22:[-4], 21:[-4], 20:[-4],
            33:[-4], 32:[-4], 31:[-4], 30:[-4],
            43:[-4], 42:[-4], 41:[-4], 40:[-4],
            53:[-4], 52:[-4], 51:[-4], 50:[-4],
            63:[-4], 62:[-4], 61:[-4], 60:[-4],
            73:[-4], 72:[-4], 71:[-4], 70:[-4],
            83:[-4], 82:[-4], 81:[-4], 80:[-4],
            93:[-4], 92:[-4], 91:[-4], 90:[-4]

        },
        'ten_-3':{
            12:[-3], 11:[-3], 10:[-3],
            22:[-3], 21:[-3], 20:[-3],
            32:[-3], 31:[-3], 30:[-3],
            42:[-3], 41:[-3], 40:[-3],
            52:[-3], 51:[-3], 50:[-3],
            62:[-3], 61:[-3], 60:[-3],
            72:[-3], 71:[-3], 70:[-3],
            82:[-3], 81:[-3], 80:[-3],
            92:[-3], 91:[-3], 90:[-3]
        },
        'ten_-2':{
            11:[-2], 10:[-2],
            21:[-2], 20:[-2],
            31:[-2], 30:[-2],
            41:[-2], 40:[-2],
            51:[-2], 50:[-2],
            61:[-2], 60:[-2],
            71:[-2], 70:[-2],
            81:[-2], 80:[-2],
            91:[-2], 90:[-2]
        },
        'ten_-1':{
            10:[-1],
            10:[-1],
            30:[-1],
            40:[-1],
            50:[-1],
            60:[-1],
            70:[-1],
            80:[-1],
            90:[-1]

        },



        // 'multi':{

        //     1:[1,2,3,4,5,6,7,8,9],
        //     2:[1,2,3,4,5,6,7,8,9],
        //     3:[1,2,3,4,5,6,7,8,9],
        //     4:[1,2,3,4,5,6,7,8,9],
        //     5:[1,2,3,4,5,6,7,8,9],
        //     6:[1,2,3,4,5,6,7,8,9],
        //     7:[1,2,3,4,5,6,7,8,9],
        //     8:[1,2,3,4,5,6,7,8,9],
        //     9:[1,2,3,4,5,6,7,8,9],

        // },
        // 'division':{
        //     1:[1,2,3,4,5,6,7,8,9],
        //     2:[1,2,3,4,5,6,7,8,9],
        //     3:[1,2,3,4,5,6,7,8,9],
        //     4:[1,2,3,4,5,6,7,8,9],
        //     5:[1,2,3,4,5,6,7,8,9],
        //     6:[1,2,3,4,5,6,7,8,9],
        //     7:[1,2,3,4,5,6,7,8,9],
        //     8:[1,2,3,4,5,6,7,8,9],
        //     9:[1,2,3,4,5,6,7,8,9],
        // }

    },
    $controllers:false,
    $list_question:false,
init: function(){
    var self = this;
        self.$controllers = $('select, input:not(#answer)');
        self.$list_question = $('.list_question');
        $('.tabs input').change(function(){
            $('.tabs-content').hide();
            $('#tabs-content__'+ this.value ).show();
            self.config.type = this.value;
        }).first().click();
        self.$controllers.change(function(){
            self.update_config();

        });
        var fquest = $("#quest-num").get(0);
        fquest.addEventListener("animationend", function () {
            $("#b-quest").removeClass('next-number');
        }, false);

        self.test.$answer = $('#answer');
        self.test.$frames = $('.b-frame');
        self.test.timer = $('.b-timer');
        self.test.quest = $('.b-quest');
        self.test.answer = $('.b-answer');
        self.test.answer_valid = $('.b-answer-valid');
        self.test.result = $('.b-result');
        self.test.$answer.keypress(function(e){
            if (e.keyCode == 13) {
                self.set_answer();
            }
        }).keyup(function(){
            only_number(this);
        });
        var selector = '[data-rangeslider]';
        var $inputRange = $(selector);

        // $(".btn-start").hide();
        $inputRange.rangeslider({
            polyfill: false,
            rangeClass: 'rangeslider',
            disabledClass: 'rangeslider--disabled',
            horizontalClass: 'rangeslider--horizontal',
            verticalClass: 'rangeslider--vertical',
            fillClass: 'rangeslider__fill',
            handleClass: 'rangeslider__handle',
            onInit: function() {
                var $element = this.$element, $rng_row = $element.parent(),
                $caption = $('<div>');
                $caption.addClass('rangeslider__caption');
                $caption.text(this.value);
                $rng_row.find('.rangeslider__handle').append($caption);
                this.$caption = $caption;
                $rng_row.append('<div class="rng-lt min">' + this.min+ '</div>');
                $rng_row.append('<div class="rng-lt max">' + this.max + '</div>');
            },
            onSlide: function(position, value) {
                this.$caption.text(value);
            },
            onSlideEnd: function(position, value) {}
        });


        self.$controllers['fs'] = {
            select_five : $('#five'),
            select_ten : $('#ten'),
            select_ext : $('#ext')
        };
        self.$controllers['fs'].select_five.select2();
        self.$controllers['fs'].select_ten.select2();
        self.$controllers['fs'].select_ext.select2();
        self.update_config();
        $.each(self.config, function(ind, val){
            if(ind.indexOf('m_')==0 || ind.indexOf('d_')==0){
                $('input[name="' + ind + '"]').first().click();
            }
        });
        $('.select-all').click(function(){

            var rule_name = $(this).data('rule'),
                selector = 'select_' + rule_name;
            if(selector in self.$controllers['fs']){
                self.$controllers['fs'][selector].children().each(function(io, eo){
                    eo.selected = true;
                });
                self.$controllers['fs'][selector].change();

            }else{
                alert('правило не найдено');
            }
            return false;
        });

        $('.btn-change-config, #test-stop').click(function(){
            self.stop_test();
            return false;
        });

        

}, update_config: function(){
    $('#reset-numbers').hide();
    var self = this;
    self.$controllers.each(function(ind, el){
        if(el.type=="radio"){
            self.config[el.name]= $('input[name="' + el.name + '"]:checked').val();//el.value;
        }else{
            self.config[el.id]= el.value;
        }
    });
    arRules = [];
    var $five = $('#five').val(), 
        $ten = $('#ten').val(),
        $ext = $('#ext').val();

    $.each($five, function(ind, el){ if(el != 5 ) arRules.push('five_' + el); });
    $.each($ten,  function(ind, el){ if(el != 10) arRules.push( 'ten_' + el); });
    $.each($ext,  function(ind, el){  arRules.push( el); });
    self.config.rules = arRules;
    
    self.exicute(function(){
        $('#reset-numbers').show();
    });
},
get_vars: function(arVars, prefix){
    var arRetVars = [];
    $.each(arVars, function (ind, el) {
        if(prefix > 0 && el > 0) arRetVars.push(el);
        else if(prefix < 0 && el < 0) arRetVars.push(el);
        else if(prefix == 0 ){
            arRetVars.push(el);
        }
    });
    if(arRetVars.length === 0) arRetVars.push(0);
    return arRetVars;
},
randomInteger: function(min, max) {
    var rand = min + Math.random() * (max - min);
    rand = Math.round(rand);
    return rand;
},

get_samples: function(){
    var self = this, arSamples =[], arRules = self.config.rules;
    $('#btn-control-error').html('');


    
    if(self.config.type=='plusminus'){
        arRules.push('none');


        arRules = arRules.filter(function(item, pos) {
            return arRules.indexOf(item) == pos;
        });


        var error = [], min_lenth = 0;
        if(arRules.length > 0){
    
            $.each(arRules, function(ind, rul){
                if(rul in self.rules){
                    $.each(self.rules[rul], function(first_number,){                   
                        var first_number_len =(Math.abs(first_number).toString()).length;
                        if(min_lenth == 0 || min_lenth > first_number_len){
                            min_lenth = first_number_len;
                        }
                    });
                }else{
                    error.push('правило ' + rul + '  не найдено в списке вариантов правил');
                }
            });



            self.min_lenth = min_lenth;
            if( self.config.len_numbers % min_lenth  !== 0){
                error.push('Разрадность для данного правила должна быть кратна ' + min_lenth +   '. Измените разрадность примерах, или правило');
            }
            if(error.length==0){
    
    
                for(quest = 0; quest < self.config.count_question; quest++){
                    arSamples.push(self.get_sample(arRules, self.config.len_numbers));
                }
                $('#btn-control-error').hide();
                $('#btn-control-start').show();
            }else{
                $.each(error, function(ie, error_str){
                    $('#btn-control-error').append('<p>' + error_str + '</p>');
                });
                $('#btn-control-error').show();
                $('#btn-control-start').hide();
            }
    
        }
    }else if(self.config.type=='multi'){
        for(quest = 0; quest < self.config.count_question; quest++){
            arSamples.push(self.get_multy(self.config.m_len_first, self.config.m_digits_first,self.config.m_len_second,self.config.m_digits_second));
        }

    }else if(self.config.type=='division'){
        for(quest = 0; quest < self.config.count_question; quest++){
            arSamples.push(self.get_division(self.config.d_len_first, self.config.d_digits_first, self.config.d_len_second, self.config.d_digits_second));
        }
    }
    self.samples = arSamples;
    return arSamples;

},


get_number: function (iVars, len) {
    var self = this, iNum ='';
    for(var n = 0; n < len; n++){
        if(iNum.length < len){
            let start_ind = 0; //(iVars[0]==0&&n==0?1:0);

            let iVarsStep= iVars;

            if(n==0){
                
                iVarsStep.splice(iVarsStep.indexOf('0'), 1);
            }

            var newNum = iVarsStep[self.randomInteger(start_ind, iVarsStep.length-1)];

           
            iNum+=''+newNum;
        }
    }
    if(iNum.length > len) iNum = iNum.substring(0, len);
    return parseInt(iNum);
},
get_random: function(len, digits, show_zero){
    show_zero = (typeof show_zero == "undefined")?true:show_zero; 
    let multy = parseInt('1'+ '0'.repeat(parseInt(len))),
        fRnd =Math.random(),
        randMill = parseInt((fRnd*100000000).toFixed(0)),
        randFl = randMill / parseInt('1' + '0'.repeat(('' + randMill).length)),
        randInt = parseFloat(((show_zero || parseInt(digits)>0?fRnd:randFl)*multy).toFixed(parseInt(digits)));
    return randInt;
},
get_multy: function(len_first, digits_first, len_second, digits_second){


    let self = this, 
        sample = {
            numbers: [
                self.get_random(len_first, digits_first),
                self.get_random(len_second, digits_second),
            ]
        };
    return sample;

},
get_division: function(len_first, digits_first, len_second, digits_second){

    let self = this, 
    sample = {
        numbers: [
            self.get_random(len_first, digits_first),
            self.get_random(len_second, digits_second, false),
        ]
    };
    return sample;
    
},
get_sample: function (rules, iLen) {
  
   
    var self = this,
        rules_number = self.rules;
    var varNumbers = {},
        firstNUmVar = [];
        let set_minus = true, set_pluse = true;
        if(rules.length > 1){
            set_minus = false;
            set_pluse = false;
            rules.forEach(function(val){
                if(val.indexOf('-')>=0)set_minus = true;
                else if(val!="none")  set_pluse = true;
                
            });
        }
    $.each(rules, function(ind, name_rule){
       
        $.each(rules_number[name_rule], function(n_num, varsN){

            // console.log(rules_number[name_rule]);
            let ar_varsN = [];

            varsN.forEach(function(val){
                ar_varsN.push(val);
                // if(set_pluse && val>=0){
                //     ar_varsN.push(val);
                // }else if(set_minus && val <=0){
                //     ar_varsN.push(val);
                // }
            });

           

            if(ar_varsN.length){
                if(n_num in varNumbers){
                    var a = varNumbers[n_num].numbers;
                    varNumbers[n_num].numbers = a.concat(ar_varsN.filter(i=>a.indexOf(i)===-1));
                    varNumbers[n_num].type = name_rule;
                    // $.each(varsN, function(_ind, _appNum){
                    //     a.concat(b.filter(i=>a.indexOf(i)===-1));
                    //     if(varNumbers[n_num].numbers.indexOf(_appNum) === -1) varNumbers[n_num].numbers.push(_appNum);
                    // });
                }else{
                    varNumbers[n_num] = {type: name_rule, numbers:ar_varsN};
                    firstNUmVar.push(n_num);
                }
            }

            
        });
    });

    var frstNum = self.get_number(firstNUmVar, iLen);

    var tmsResult = frstNum,
        sample = {
            numbers: [frstNum],
            type:'',
            steps: {
                'first_number':frstNum
            }
        };
        
    for(var num = 0; num < self.config.count_numbers - 1; num++){

      
        var scndNum = self.get_nxt_number(tmsResult, varNumbers, self.config.len_numbers, set_minus, set_pluse);
        sample.type=scndNum.type;
        sample.numbers.push(scndNum.numbers);
        let beforeResult = parseInt(tmsResult);
        tmsResult = parseInt(tmsResult) + parseInt(scndNum.numbers);
        sample.steps[num +'_number'] = {
            'variants': varNumbers,
            'len_number':self.config.len_numbers,
            'return':scndNum,
            'before_result':beforeResult,
            'timed_result':tmsResult
        };
    }

    
   
    return sample;



},
exicute: function (fn) {
    var self = this;
    self.$list_question.html('');
    var arSamples = self.get_samples();
    if(arSamples.length > 0)
        $.each(arSamples, function (ind_sample, ar_quest) {
            var $quest= $('<div>');
            var s_simple ='';

            if(self.config.type == "multi" ){
                s_simple = ar_quest.numbers.join('*');
            }else if(self.config.type == "division"){
                 s_simple = ar_quest.numbers.join('/');
            }else{
                 s_simple = ar_quest.numbers.join('+');
                 s_simple =s_simple.replace(/\+\-/g, '-');
            }

            $quest.html(ind_sample + ") "+s_simple + '=' +self.get_result(ar_quest) );
            self.$list_question.append($quest);
        });
    if(typeof fn == "function") fn();
},

get_result: function (ar_quest) {
    var self=this, answer = 0;

    $.each(ar_quest.numbers, function(ind, el){
        second_num = parseFloat(el);
        if(self.config.type=="multi"){
            if(ind==0){
                answer = second_num;
            }else{
                answer = answer * second_num;
            }
        }
        else if(self.config.type=="division"){
            if(ind==0){
                answer = (second_num);
            }else{
                answer = answer/second_num;
            }
        }else{
            answer+=second_num;
        }
    });
    return parseFloat(answer.toFixed(3));
},

get_nxt_number:function(tmsResult, varNumbers, len_numbers, set_minus, set_pluse){

    var self = this, scndNum ='',
        pref = (tmsResult < 0? '-':''),
        modTmsResult = (tmsResult +'').replace('-', ''),
        tmp_modTmsResult = modTmsResult,
        nxt_num ='';

    var reverseVariant = function(o_data){
        var data = $.parseJSON(JSON.stringify(o_data)),
            arr = [];
        for( var ind in data ) {
            if (data[ind]) {
                arr[ind] = $.extend({sind: ind},data[ind]);
            }
        }
       
        var len = arr.length,
            newVar = [];
        while( len-- ) {
            if( arr[len] !== undefined ) {
                newVar[len] = arr[len];
            }
        }
        return newVar.reverse();
    },
        allVariants = reverseVariant(varNumbers);


    let getListIdx = function (str, substr) {
        let listIdx = [];
        let lastIndex = -1;
        while ((lastIndex = str.indexOf(substr, lastIndex + 1)) !== -1) {
          listIdx.push(lastIndex)
        }
        return listIdx
      }, arSplitResult = {}, oSplitResultVars = {};

    $.each(allVariants,  function(ind_first, ar_varians){
        if(ar_varians){
            let searcheble_indexes = getListIdx(tmp_modTmsResult, ar_varians.sind);
            if(searcheble_indexes.length>0){
                searcheble_indexes.forEach((val)=>{
                    arSplitResult[val] = ar_varians.sind;
                });
                let repReg = new RegExp(ar_varians.sind, 'g');
                tmp_modTmsResult = tmp_modTmsResult.replace(repReg, " ".repeat(ar_varians.sind.length));
                oSplitResultVars[ar_varians.sind] = ar_varians;
            }
        }
    });

    if(tmp_modTmsResult.trim().length>0){
        for(let ne=0;ne<tmp_modTmsResult.length;ne++){
            if(tmp_modTmsResult[ne]!=' '){
                arSplitResult[ne] = tmp_modTmsResult[ne];
                oSplitResultVars[tmp_modTmsResult[ne]] = {numbers:[0]};
            }
        }
    }



    let gen_num_fn = function (split_arr, oSplitResultVars, type_oper, max_len=6) {
        let str_gen_number='', ar_gen_num = [], o_gen_num = [];
        let w_zero_val  = [],
            str_len = split_arr.join('').replace(/-/g, '').length-1;

        if(str_len < max_len){
            while(w_zero_val.length < (max_len - str_len - 1)){
                w_zero_val.push(0);
            }
            split_arr = w_zero_val.concat(split_arr);
            oSplitResultVars[0] = { numbers: [1,2,3,4,5]};
        }

        let spArLen = split_arr.length - 1;
        let f_gen_num = '', f_s_gen_num = '';

        if(type_oper==0){
            let search_num = split_arr[0];
            let arVars = oSplitResultVars[search_num].numbers;
            
            f_gen_num = arVars[self.randomInteger(0, arVars.length - 1)];
            type_oper = (f_gen_num>0?1:f_gen_num<0?-1:0);
            if(('' + search_num).length > (''+Math.abs(f_gen_num)).length){
                let count_null = ('' + search_num).length - (''+Math.abs(f_gen_num)).length;
                f_s_gen_num =   '0'.repeat(count_null) + ('' + Math.abs(f_gen_num));
            }else{
                f_s_gen_num =    '' + Math.abs(f_gen_num);
            }
        }
        
        for(let l=spArLen; l>=0; l--){
            if((ar_gen_num.join('').length + f_s_gen_num.length) < max_len){
                let gen_num;
                let search_num = split_arr[l];
                let arVars =[];
                arVars = self.get_vars(oSplitResultVars[search_num].numbers, type_oper);
                gen_num = arVars[self.randomInteger(0, arVars.length - 1)];

                // console.log(oSplitResultVars);
                // console.log(search_num);
                // console.log(arVars);
                // console.log(gen_num);


                    let s_gen_num = '';
                    let pref_pm = (type_oper < 0 && ar_gen_num.length == 0)?'-':'';

                    if(('' + search_num).length > (''+Math.abs(gen_num)).length){
                        let count_null = ('' + search_num).length - (''+Math.abs(gen_num)).length;

                        // console.log(' count_null ' + count_null);
                        s_gen_num =   '0'.repeat(count_null) + ('' + Math.abs(gen_num));
                    }else{
                        s_gen_num =    '' + Math.abs(gen_num);
                    }
                    o_gen_num.push({s:search_num, g: gen_num});


                if(type_oper==0) type_oper = (gen_num>0?1:gen_num<0?-1:0);

                ar_gen_num.push(s_gen_num);
            }
        }
        ar_gen_num.push(Math.abs(f_s_gen_num));
        o_gen_num.push({s:split_arr[0], g: f_s_gen_num, abs: Math.abs(f_s_gen_num)});


        

        let arReverse = [], 
            len = ar_gen_num.length ;

        // console.log(ar_gen_num);
        while( len-- ) {
        
            if( ar_gen_num[len] !== undefined ) {

                // console.log('---------------' + ar_gen_numlen);
                arReverse.push( '' + ar_gen_num[len]);
            }
        }

        // console.log(arReverse);

        str_gen_number = ((type_oper< 0)?'-':'') + arReverse.join('').replace(/\//g,'');
        return {
            split_arr: split_arr,
            data: o_gen_num,
            reverse: arReverse,
            value: str_gen_number
        };
    };





    let  split_arr = Object.values(arSplitResult),
        type_oper = (set_minus!=set_pluse)?
            (
                (set_minus && split_arr[0]!=1)?-1:set_pluse?1:0
            ):0
        , o_gen_num = gen_num_fn(split_arr, oSplitResultVars, type_oper, len_numbers)
        , str_gen_number= o_gen_num.value;
    // console.log(split_arr, oSplitResultVars);


    if(parseInt(str_gen_number)==0){
        o_gen_num =  gen_num_fn(split_arr, oSplitResultVars, 0, len_numbers);
        str_gen_number  = str_gen_number= o_gen_num.value;
    }


    return {
        type: 'none',
        split: oSplitResultVars,
        varNumbers:varNumbers,
        ar_select: o_gen_num,
        numbers:parseInt(str_gen_number),
        // 'vars':arVars
    };
},
// test functions
    timer: function(){
        var self = this, $count_sec  = $('.count-second');
        $count_sec.text(self.test.time_to_start);
        setTimeout(function () {

            if(self.test_inprogress){
                self.test.time_to_start--;
                if(self.test.time_to_start>0){
                    self.timer();
                    $count_sec.text(self.test.time_to_start);
                }else{
                    self.show_quest();
                }
            }



        }, 1000);
    },

    change_frame: function(frame_name){
        var self = this;
        self.test.$frames.removeClass(self.classes.active);
        self.test[frame_name].addClass(self.classes.active);
        var inputs = self.test[frame_name].find('input');
        inputs.each(function(inp_d, inp_){
            inp_.value ='';
            inp_.focus();
        });
        return self.test[frame_name];
    },
    start_test: function(){
        var self = this;


        console.log('start test');
        self.test.time_to_start = 5;
        self.test.number_quest = 0;
        self.test_inprogress = true;
        self.test.answers = {};
        $('.content').addClass('test');
        self.change_frame('timer');
        self.timer();
    },

    show_quest: function(){

        console.log('show_quest');
        var  self = this;

        if( self.test_inprogress){
            self.change_frame('quest');
            var num_num = 0,
                quest = self.samples[self.test.number_quest],
                count_num = quest.numbers.length;
            $('#quest-num').html("");
            self.show_number(quest.numbers[num_num]);
            var showing_quest = setInterval(function () {
                // if(self.test_inprogress ){
                    num_num++;
                    num_num = self.get_next_num(quest.numbers, num_num);
                    if(  num_num<count_num){
                        var operation = "";
                        if(self.config.type=="multi"){
                            operation = "*";
                        }else if(self.config.type=="division"){
                            operation = "/";
                        }else if(parseInt(quest.numbers[num_num])<0){
                            operation = "";
                        }
                        self.show_number(operation + quest.numbers[num_num]);
                    }else{
                        clearInterval(showing_quest);
                        self.change_frame('answer');
                        var correct_answer = self.get_result(quest),
                            maxlen = (correct_answer + '').length;
                        var old_answer = self.test.number_quest;
                        self.test.timer_set_answer = self.config.time_answer;
                        self.test.fn_timer_set_answer = setInterval(function () {
                            self.test.timer_set_answer-=.1;
                            if(self.test.timer_set_answer <= 0){
                                self.set_answer();
                            }
                        }, 100);
                    }

            }, ((parseFloat(self.config.time_pause) )*1000 + 500) );
        }else{
            clearInterval(showing_quest);
        }
    },
    get_next_num:function(numbers, num_num){
        var play_for = true;
        for(var n = num_num; n<=numbers.length; n++){
            if(play_for){
                if(parseInt(numbers[n])!==0) play_for = false;
                num_num = n;
            }
        }
        return num_num;
    },
    show_number: function(num){
        console.log('show_number');
        $('#quest-num').html(num);
    },
    get_sample_all: function(ar_quest){
        var s_sample = '';
        $.each(ar_quest, function(ind, el){
            var int_el  = parseInt(el);
            s_sample+=(int_el<0?int_el:(s_sample!=''?'+':'')+int_el);
        });
        return s_sample;
    },
    set_answer: function(){
        var self = this,
            quest = self.samples[self.test.number_quest],
            correct_answer = self.get_result(quest),
            s_sample = self.get_sample_all(quest);
        clearInterval(self.test.fn_timer_set_answer);


        var user_answer = self.test.$answer.val();

        var $frame = self.change_frame('answer_valid');
        var $res_ans = $('.result-answer'),
            $res_ans_img = $('.result-answer-image');


        self.test.answers[self.test.number_quest] = {
            sample: s_sample,
            correct: correct_answer,
            user: user_answer,
            valid: (correct_answer == user_answer)
        };

        var txt_res_ans = '';



        //@todo: add img add class

        $res_ans_img.removeClass('correct error');
        if(self.test.answers[self.test.number_quest].valid){
            $frame.removeClass('f-frame-error');
            $frame.addClass('f-frame-success');
            $res_ans.html('Верно');
            $res_ans.removeClass('error');
            txt_res_ans = correct_answer  + ' = '+ user_answer;
            $res_ans_img.addClass('correct');
        }else{
            $frame.removeClass('f-frame-success');
            $frame.addClass('f-frame-error');
            $res_ans.html('Не верно');
            $res_ans.addClass('error');
            txt_res_ans = correct_answer  + ' ≠ '+ user_answer;
            $res_ans_img.addClass('error');
        }
        $('.result-answer-value').html(txt_res_ans);
        if(self.test_inprogress) {
            setTimeout(function () {
                self.test.number_quest++;
                if (self.test_inprogress) {
                    if (self.test.number_quest < self.config.count_question) {
                        self.show_quest();
                    } else {
                        self.change_frame('result');
                        self.get_statistic();
                    }
                }
            }, 2000);
        }
    },
    get_statistic: function(){

        var self = this,
            count_answers = 0,
            valid_answer = 0,
            error_answer = 0;

        clearInterval(self.test.fn_timer_set_answer);

        self.test_inprogress = false;
        $.each(self.test.answers, function(ind_a, ans){

            // console.log(ind_a);
            // console.log(ans);
            if(ans.valid){
                valid_answer++;
            }else{
                error_answer++;
            }
            count_answers++;
        });
        var percent_result = Math.round((valid_answer/count_answers)*100);

        $('.count_samples').html(count_answers);
        $('.count_corrects').html(valid_answer);
        $('.count_error').html(error_answer);
        $('.result-percent').html(percent_result + '%');


        var $id_user = $('#id_user'), $id_task = $('#id_task');
        if($id_user.length && $id_task.length){
            var arResult = {
                id_user:$id_user.val(),
                id_task: $id_task.val(),
                valid_answer: valid_answer,
                error_answer: error_answer,
                percent_result: percent_result,
                test: oSampler.test.answers
            };
            // console.log(arResult);
            axios.post('/api/task.php', {
                data:arResult,
                action:'add-task-user',
            }).then(response => {
                // console.log(response);
                // this.user = response.data;
                // this.load_data_user = true;
            }).catch((error) => {
                // console.log(error);
                this.errored = true;
            }).finally(() => (console.log('finally')));

        }
    },
    stop_test: function(){
        // self.change_frame('answer_valid');
        // self.timer();
        var self = this;
        self.test.time_to_start = 5;
        self.test.number_quest = 0;
        self.test_inprogress = false;
        clearInterval(self.test.fn_timer_set_answer);
        $('.content').removeClass('test');
    }
};