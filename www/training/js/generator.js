String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};
var only_number = function (input) {
    var value = input.value;
    var re = /[^0-9\-]/gi;
    if (re.test(value)) {
        value = value.replace(re, '');
        input.value = value;
    }
};
var oSampler = {
    classes: {
        active: 'active'
    },
    config:{
        time_pause: 1,
        time_answer: 7,
        count_question: 0,
        count_numbers: 0,
        len_numbers: 0,
        rules:[]
    },
    samples: [],
    min_lenth: 1,
    test:{
        time_to_start: 0,
        number_quest: 0,
        answers:{},
        timer_set_answer: 0,
        fn_timer_set_answer: false,
        play: false
    },
    rules:{
        'none':{
            1: [1,2,3,5,6,7,8],
            2: [1,2,5,6,7, -1],
            3: [1,5,6,-1,-2],
            4: [5,-1,-2,-3],
            5: [1,2,3,4],
            6: [1,2,3,-1,-5],
            7: [1,2,-1,-2, -5],
            8: [1,-1,-2,-3, -5],
            9: [-1,-2,-3,-4,-5]
        },
        'five_-4':{
            5:[-4], 6: [-4], 7: [-4], 8: [-4]
        },
        'five_-3':{
            5:[-3], 6:[-3], 7:[-3]
        },
        'five_-2':{
            5:[-2], 6:[-2]
        },
        'five_-1':{
            5:[-1]
        },
        'five_1':{
            4:[1]
        },
        'five_2':{
            3:[2], 4:[2]
        },
        'five_3':{
            2:[3], 3:[3], 4:[3]
        },
        'five_4':{
            1:[4], 2:[4], 3:[4], 4:[4]
        },
        'ten_9':{1:[9],2:[9],3:[9],4:[9],5:[9],6:[9],7:[9],8:[9],9:[9],},
        'ten_8':{2:[8],3:[8],4:[8],5:[8],6:[8],7:[8],8:[8],9:[8],},
        'ten_7':{3:[7],4:[7],5:[7],6:[7],7:[7],8:[7],9:[7],},
        'ten_6':{4:[6],5:[6],6:[6],7:[6],8:[6],9:[6],},
        'ten_5':{5:[5],6:[5],7:[5],8:[5],9:[5],},
        'ten_4':{6:[4],7:[4],8:[4],9:[4]},
        'ten_3':{7:[3],8:[3],9:[3]},
        'ten_2':{9:[2], 8:[2]},
        'ten_1':{9:[1]},

        'ten_-9':{18:[-9], 17:[-9],16:[-9], 15:[-9], 14:[-9], 13:[-9], 12:[-9], 11:[-9], 10:[-9]},
        'ten_-8':{17:[-8], 16:[-8], 15:[-8], 14:[-8], 13:[-8], 12:[-8], 11:[-8], 10:[-8]},
        'ten_-7':{16:[-7], 15:[-7], 14:[-7], 13:[-7], 12:[-7], 11:[-7], 10:[-7]},
        'ten_-6':{15:[-6], 14:[-6], 13:[-6], 12:[-6], 11:[-6], 10:[-6]},
        'ten_-5':{14:[-5], 13:[-5], 12:[-5], 11:[-5], 10:[-5]},
        'ten_-4':{13:[-4], 12:[-4], 11:[-4], 10:[-4]},
        'ten_-3':{12:[-3], 11:[-3], 10:[-3]},
        'ten_-2':{11:[-2], 10:[-2]},
        'ten_-1':{10:[-1]},
        'multi':{

            1:[1,2,3,4,5,6,7,8,9],
            2:[1,2,3,4,5,6,7,8,9],
            3:[1,2,3,4,5,6,7,8,9],
            4:[1,2,3,4,5,6,7,8,9],
            5:[1,2,3,4,5,6,7,8,9],
            6:[1,2,3,4,5,6,7,8,9],
            7:[1,2,3,4,5,6,7,8,9],
            8:[1,2,3,4,5,6,7,8,9],
            9:[1,2,3,4,5,6,7,8,9],

        },
        'division':{
            1:[1,2,3,4,5,6,7,8,9],
            2:[1,2,3,4,5,6,7,8,9],
            3:[1,2,3,4,5,6,7,8,9],
            4:[1,2,3,4,5,6,7,8,9],
            5:[1,2,3,4,5,6,7,8,9],
            6:[1,2,3,4,5,6,7,8,9],
            7:[1,2,3,4,5,6,7,8,9],
            8:[1,2,3,4,5,6,7,8,9],
            9:[1,2,3,4,5,6,7,8,9],
        }
        
    },
    $controllers:false,
    $list_question:false,
    randomInteger: function(min, max) {
        var rand = min + Math.random() * (max - min);
        rand = Math.round(rand);
        return rand;
    },
    get_vars: function(arVars, prefix){
        var arRetVars = [];
        $.each(arVars, function (ind, el) {
            if(prefix>0 && el > 0) arRetVars.push(el);
            else if(prefix < 0 && el < 0) arRetVars.push(el);
        });
        if(arRetVars.length ===0) arRetVars.push(0);
        return arRetVars;
    },
    get_number: function (iVars, len) {
        var self = this, iNum ='';
        // console.log('*****************iVars***************');
        // console.log(iVars);
        for(var n = 0; n < len; n++){
            if(iNum.length < len){
                var newNum = iVars[self.randomInteger(0, iVars.length-1)];

                iNum+=''+newNum;
            }
        }
        if(iNum.length > len) iNum = iNum.substring(0, len);
        return parseInt(iNum);
    },
    get_nxt_number:function(tmsResult, varNumbers){

        // console.log('<fn--get_nxt_number--fn>');
        var self = this, scndNum ='';
        var pref = (tmsResult < 0? '-':''),
            modTmsResult =  (tmsResult +'').replace('-', ''),
            nxt_num ='';


        // console.log(tmsResult);
        // console.log(modTmsResult);

        for(var l = 0; l < self.config.len_numbers; l+= self.min_lenth){


            var num_sum = '',  aVars= [0];

                for(var n = l; n < l + self.min_lenth; n++ ){
                    num_sum+= modTmsResult.charAt(n);
                }


                if(num_sum in varNumbers) {
                    if(num_sum === '0'){
                        aVars = [1,2,3,4,5];
                    }
                    else if(l === 0) aVars = varNumbers[num_sum].numbers;
                    else if(parseInt(scndNum)<1){
                        aVars = self.get_vars(varNumbers[num_sum].numbers, -1);
                    }
                    else if(parseInt(scndNum)>1){
                        aVars = self.get_vars(varNumbers[num_sum].numbers, 1);
                    }
                }
                nxt_num =(aVars.length>0? (aVars.length === 1 ? aVars[0] : aVars[self.randomInteger(0, aVars.length - 1)]):0);


                if(typeof nxt_num == 'undefined') nxt_num = 0;


                var nxt_num_len = (nxt_num + '').toString().replace('-', '').length,
                    is_minus = (nxt_num.toString().charAt(0) === '-'),
                    tmp_nxt_num  =nxt_num.toString().replace('-', '');




                if(nxt_num_len < num_sum.length){

                    for (zero_n = 0; zero_n < num_sum.length - nxt_num_len; zero_n++){
                        tmp_nxt_num = '0' + tmp_nxt_num;
                    }
                }else{
                    // console.log(num_sum.length );
                    // console.log('nxt_num_len ' + nxt_num_len);
                }

                nxt_num = (is_minus?'-' + tmp_nxt_num : tmp_nxt_num);


                scndNum+= '' +  nxt_num;

                var scndNum_len = scndNum.replace('-', '').length,
                    is_minus = (scndNum.charAt(0) === '-'),
                    tmp_scndNum = scndNum.replace('-', '');


            //     if(scndNum_len < num_sum.length){
            //
            //         for (zero_n = 0; zero_n <= num_sum.length - scndNum.length; zero_n++){
            //             tmp_scndNum = '0' + tmp_scndNum;
            //         }
            //     }else{
            //         console.log(scndNum_len);
            //         console.log(num_sum.length);
            //         console.log('ADD ZERO: ' + (scndNum_len < num_sum.length))
            //     }
            //
            // scndNum = parseInt(is_minus?'-'+tmp_scndNum: tmp_scndNum).toString();


            if(scndNum.charAt(0) === '-') scndNum = '-' + scndNum.replaceAll('-', '');

            scndNum  = parseInt(scndNum);

           // scndNum = scndNum.replaceAll(/0\-/, '-');
            // console.log('num_sum ' + num_sum);
        }


        // console.log('varNumbers');
        // console.log(varNumbers);

        // console.log('scndNum ' + scndNum);

        // console.log('</fn--get_nxt_number--fn>');

        if(varNumbers[num_sum]){
            return {
                type: varNumbers[num_sum].type,
                numbers:scndNum
            };
        }
        else{
            return {
                type: 'none',
                numbers: 0
            };
        }

    },
    get_sample: function (rules, iLen) {
        // console.log('<FNFNFNFNFNFNFN---get_sample---FNFNFNFNFNFNFN>');
        var self = this,
            rules_number = self.rules;
        var varNumbers = {},
            firstNUmVar = [];


        // console.log(rules);
        $.each(rules, function(ind, name_rule){

            // console.log('[[[[name_rule ' + name_rule);
            $.each(rules_number[name_rule], function(n_num, varsN){

                // console.log(n_num);
                // console.log(varsN);


                if(n_num in varNumbers){
                    $.each(varsN, function(_ind, _appNum){
                        if(varNumbers[n_num].numbers.indexOf(_appNum) === -1) varNumbers[n_num].numbers.push(_appNum);
                    });
                }else{
                    varNumbers[n_num] = {
                        type: name_rule,
                        numbers:varsN
                    };
                    firstNUmVar.push(n_num);
                }
            });
        });
        // console.log('varNumbers');
        // console.log(varNumbers);

        var frstNum = self.get_number(firstNUmVar, iLen);

        // console.log('iLen ' + iLen);
        // console.log('firstNUmVar ' + firstNUmVar);
        // console.log('frstNum ' + frstNum);

        var tmsResult = frstNum,
            sample = {
                numbers: [frstNum],
                type:''
            };
        for(var num = 0; num < self.config.count_numbers - 1; num++){

            // console.log('num ' +num);
            // console.log('tmsResult ' +tmsResult);
            var scndNum = self.get_nxt_number(tmsResult, varNumbers, self.config.len_numbers);
            sample.type=scndNum.type;
            sample.numbers.push(scndNum.numbers);
            tmsResult = parseInt(tmsResult) + parseInt(scndNum.numbers);
        }
        // console.log(sample);
        // console.log('</FNFNFNFNFNFNFN---get_sample---FNFNFNFNFNFNFN>');
        return sample;

    },
    init: function(){
        var self = this;
        self.$controllers = $('select, input:not(#answer)');
        self.$list_question = $('.list_question');
        self.$controllers.change(function(){

            // console.log('self.$controllers.change');
            self.update_config();

        });


        var fquest = $("#quest-num").get(0);
        fquest.addEventListener("animationend", function () {

            // console.log('animationend');
            $("#b-quest").removeClass('next-number');
        }, false);



        self.test.$answer = $('#answer');
        self.test.$frames = $('.b-frame');
        self.test.timer = $('.b-timer');
        self.test.quest = $('.b-quest');
        self.test.answer = $('.b-answer');
        self.test.answer_valid = $('.b-answer-valid');
        self.test.result = $('.b-result');
        self.test.$answer.keypress(function(e){
            if (e.keyCode == 13) {
                self.set_answer();
            }
        }).keyup(function(){
            only_number(this);
        });
        var selector = '[data-rangeslider]';
        var $inputRange = $(selector);
        $inputRange.rangeslider({
            polyfill: false,
            // Default CSS classes
            rangeClass: 'rangeslider',
            disabledClass: 'rangeslider--disabled',
            horizontalClass: 'rangeslider--horizontal',
            verticalClass: 'rangeslider--vertical',
            fillClass: 'rangeslider__fill',
            handleClass: 'rangeslider__handle',
            onInit: function() {
                var $element = this.$element, $rng_row = $element.parent(),
                $caption = $('<div>');
                $caption.addClass('rangeslider__caption');
                $caption.text(this.value);
                $rng_row.find('.rangeslider__handle').append($caption);
                this.$caption = $caption;
                $rng_row.append('<div class="rng-lt min">' + this.min+ '</div>');
                $rng_row.append('<div class="rng-lt max">' + this.max + '</div>');
            },
            onSlide: function(position, value) {
                this.$caption.text(value);
            },
            onSlideEnd: function(position, value) {}
        });


        self.$controllers['fs'] = {
            select_five : $('#five'),
            select_ten : $('#ten'),
            select_ext : $('#ext')
        };

        // self.$controllers['fs'].select_five.fastselect();
        // self.$controllers['fs'].select_ten.fastselect();
        // self.$controllers['fs'].select_ext.fastselect();

        self.$controllers['fs'].select_five.select2();
        self.$controllers['fs'].select_ten.select2();
        self.$controllers['fs'].select_ext.select2();


        self.update_config();

        $('.select-all').click(function(){

            var rule_name = $(this).data('rule'),
                selector = 'select_' + rule_name;
            if(selector in self.$controllers['fs']){
                // console.log(self.$controllers['fs'][selector]);
                self.$controllers['fs'][selector].children().each(function(io, eo){

                    eo.selected = true;
                    // console.log(eo);
                });
                self.$controllers['fs'][selector].change();

            }else{
                alert('правило не найдено');
            }
            return false;
        });

        $('.btn-change-config, #test-stop').click(function(){
            self.stop_test();
            return false;
        });
    },
    update_config: function(){
        var self = this;
        self.$controllers.each(function(ind, el){
            self.config[el.id]= el.value;
        });
        arRules = [];
        var $five = $('#five').val(), 
            $ten = $('#ten').val(),
            $ext = $('#ext').val();

        $.each($five, function(ind, el){ if(el != 5 ) arRules.push('five_' + el); });
        $.each($ten,  function(ind, el){ if(el != 10) arRules.push( 'ten_' + el); });
        $.each($ext,  function(ind, el){  arRules.push( el); });




        self.config.rules = arRules;

        // console.log('$$$$$$$$$$$arRules$$$$$$$$$');
        // console.log(arRules);
        self.exicute();
    },
    get_sample_all: function(ar_quest){
        var s_sample = '';
        $.each(ar_quest, function(ind, el){
            var int_el  = parseInt(el);


            s_sample+=(int_el<0?int_el:(s_sample!=''?'+':'')+int_el);
        });
        return s_sample;
    },
    get_result: function (ar_quest) {
        var answer = 0;


        // console.log('get_result');
        // console.log('ar_quest:');
        // console.log(ar_quest);
        $.each(ar_quest.numbers, function(ind, el){


            second_num = parseInt(el);

            // console.log('ind:' + ind);
            // console.log('second_num:');
            // console.log(ar_quest.type);
            // console.log(second_num);
            if(ar_quest.type=="multi"){

                if(ind==0){
                    answer = second_num;
                }else{
                    answer = answer * second_num;
                }

            }
            else if(ar_quest.type=="division"){
                if(ind==0){
                    answer = second_num;
                }else{
                    answer = answer/second_num;
                }
            }else{
                answer+=second_num;
            }
        });
        return answer;
    },
    get_samples: function(){

        var self = this, arSamples =[], arRules = self.config.rules;

        $('#btn-control-error').html('');

        // console.log('=========get_samples========';
        // console.log('rules');
        // console.log(arRules);


        if(arRules.length === 0) arRules.push('none');

        var error = [], min_lenth = 0;
        if(arRules.length > 0){

            $.each(arRules, function(ind, rul){
                if(rul in self.rules){
                    $.each(self.rules[rul], function(first_number,){

                        // console.log(first_number);
                        var first_number_len =(Math.abs(first_number).toString()).length;


                        // console.log('first_number_len ' + first_number_len);



                        if(min_lenth == 0 || min_lenth > first_number_len){
                            min_lenth = first_number_len;
                        }
                    });
                }else{
                    error.push('правило ' + rul + '  не найдено в списке вариантов правил');
                }
            });
            self.min_lenth = min_lenth;
            if( self.config.len_numbers % min_lenth  !== 0){
                error.push('Разрадность для данного правила должна быть кратна ' + min_lenth +   '. Измените разрадность примерах, или правило');
            }

            // console.log('min_lenth + ' + min_lenth);
            // console.log('len_numbers + ' + self.config.len_numbers);

            if(error.length==0){


                for(quest = 0; quest < self.config.count_question; quest++){
                    arSamples.push(self.get_sample(arRules, self.config.len_numbers));
                }
                $('#btn-control-error').hide();
                $('#btn-control-start').show();
            }else{
                $.each(error, function(ie, error_str){
                    $('#btn-control-error').append('<p>' + error_str + '</p>');
                });
                $('#btn-control-error').show();
                $('#btn-control-start').hide();
            }

        }

        // console.log('---------------samples------------------');
        // console.log(arSamples);
        self.samples = arSamples;
        return arSamples;
    },
    exicute: function () {
        var self = this;
        self.$list_question.html('');


        var arSamples = self.get_samples();
        if(arSamples.length > 0)
            $.each(arSamples, function (ind_sample, ar_quest) {
                var $quest= $('<div>');

                var s_simple ='';

                // console.log(ar_quest);


                if(ar_quest.type == "multi" ){
                    s_simple = ar_quest.numbers.join('*');
                }else if(ar_quest.type == "division"){
                     s_simple = ar_quest.numbers.join('/');
                }else{
                     s_simple = ar_quest.numbers.join('+');
                     s_simple =s_simple.replaceAll(/\+\-/, '-');
                }

                $quest.html(s_simple + '=' +self.get_result(ar_quest) );
                self.$list_question.append($quest);
            });
    },
    timer: function(){
        var self = this,
            $count_sec  = $('.count-second');

        $count_sec.text(self.test.time_to_start);
        setTimeout(function () {
            self.test.time_to_start--;
            if(self.test.time_to_start>0){
                self.timer();
                $count_sec.text(self.test.time_to_start);
            }else{
                self.show_quest();
                //self.change_frame('quest');
            }
        }, 1000);
    },
    show_number: function(num){


        setTimeout(function () {
            setTimeout(function() {
                $('#quest-num').html(num);
            }, 250);
            $('.b-quest').addClass('next-number');
        }, 10);

    },
    get_next_num:function(numbers, num_num){

        var play_for = true;
        for(var n = num_num; n<=numbers.length; n++){
            if(play_for){
                if(parseInt(numbers[n])!==0) play_for = false;
                num_num = n;
            }
        }
        // console.log(num_num);
        return num_num;

    },
    show_quest: function(){
        var self = this;

        if( self.test.play){
            self.change_frame('quest');
            var num_num = 0,
                quest =self.samples[self.test.number_quest],
                count_num = quest.numbers.length;

            $('#quest-num').html("");


            
            self.show_number(quest.numbers[num_num]);

            var showing_quest = setInterval(function () {
                if(self.test.play ){
                    num_num++;
                    num_num = self.get_next_num(quest.numbers, num_num);
                    if(  num_num<count_num){
                        var operation = "";
                        if(quest.type=="multi"){
                            operation = "*";
                        }else if(quest.type=="division"){
                            operation = "/";
                        }else if(parseInt(quest.numbers[num_num])<0){
                            operation = "";
                        }
                        self.show_number(operation + quest.numbers[num_num]);
                    }else{
                        clearInterval(showing_quest);
                        self.change_frame('answer');
                        var correct_answer = self.get_result(quest),
                            maxlen = (correct_answer + '').length;
                        var old_answer = self.test.number_quest;

                        self.test.timer_set_answer = self.config.time_answer;
                        self.test.fn_timer_set_answer = setInterval(function () {
                            self.test.timer_set_answer-=.1;
                            if(self.test.timer_set_answer <= 0){
                                self.set_answer();
                            }
                        }, 100);
                    }
                }else{
                    clearInterval(showing_quest);

                }
            }, ((parseFloat(self.config.time_pause) )*1000 + 500) );
        };
    },
    get_statistic: function(){

        var self = this,
            count_answers = 0,
            valid_answer = 0,
            error_answer = 0;

        self.test.play = false;
        $.each(self.test.answers, function(ind_a, ans){

            console.log(ind_a);
            console.log(ans);
            if(ans.valid){
                valid_answer++;
            }else{
                error_answer++;
            }
            count_answers++;
        });
        var percent_result = Math.round((valid_answer/count_answers)*100);
        
        $('.count_samples').html(count_answers);
        $('.count_corrects').html(valid_answer);
        $('.count_error').html(error_answer);
        $('.result-percent').html(percent_result + '%');


        var $id_user = $('#id_user'), $id_task = $('#id_task');
        if($id_user.length && $id_task.length){
            var arResult = {
                id_user:$id_user.val(),
                id_task: $id_task.val(),
                valid_answer: valid_answer,
                error_answer: error_answer,
                percent_result: percent_result,
                test: oSampler.test.answers
            };
            // console.log(arResult);
            axios.post('/api/task.php', {
                data:arResult,
                action:'add-task-user',
            }).then(response => {
                // console.log(response);
                // this.user = response.data;
                // this.load_data_user = true;
            }).catch((error) => {
                // console.log(error);
                this.errored = true;
            }).finally(() => (console.log('finally')));

        }
    },
    set_answer: function(){
        var self = this,
            quest = self.samples[self.test.number_quest],
            correct_answer = self.get_result(quest),
            s_sample = self.get_sample_all(quest);


        clearInterval(self.test.fn_timer_set_answer);
        var user_answer = self.test.$answer.val();

        var $frame = self.change_frame('answer_valid');
        var $res_ans = $('.result-answer'),
            $res_ans_img = $('.result-answer-image');


        self.test.answers[self.test.number_quest] = {
            sample: s_sample,
            correct: correct_answer,
            user: user_answer,
            valid: (correct_answer == user_answer)
        };

        var txt_res_ans = '';



        //@todo: add img add class

        $res_ans_img.removeClass('correct error');
        if(self.test.answers[self.test.number_quest].valid){
            $frame.removeClass('f-frame-error');
            $frame.addClass('f-frame-success');
            $res_ans.html('Верно');
            $res_ans.removeClass('error');
            txt_res_ans = correct_answer  + ' = '+ user_answer;
            $res_ans_img.addClass('correct');
        }else{
            $frame.removeClass('f-frame-success');
            $frame.addClass('f-frame-error');
            $res_ans.html('Не верно');
            $res_ans.addClass('error');
            txt_res_ans = correct_answer  + ' ≠ '+ user_answer;
            $res_ans_img.addClass('error');
        }
        $('.result-answer-value').html(txt_res_ans);
        setTimeout(function () {
            self.test.number_quest++;
            if(self.test.number_quest < self.config.count_question ){
                self.show_quest();
            }else{
                self.change_frame('result');
                self.get_statistic();
            }
        }, 2000);
    },
    change_frame: function(frame_name){
        var self = this;
        self.test.$frames.removeClass(self.classes.active);
        self.test[frame_name].addClass(self.classes.active);
        var inputs = self.test[frame_name].find('input');
        inputs.each(function(inp_d, inp_){
            inp_.value ='';
            inp_.focus();
        });
        return self.test[frame_name];
    },
    start_test: function(){
        var self = this;
        self.test.time_to_start = 5;
        self.test.number_quest = 0;
        self.test.play = true;
        self.test.answers = {};
        $('.content').addClass('test');
        self.change_frame('timer');
        self.timer();
    },
    stop_test: function(){
        // self.change_frame('answer_valid');
        // self.timer();
        var self = this;
        self.test.time_to_start = 5;
        self.test.number_quest = 0;
        self.test.play = false;
        $('.content').removeClass('test');

    }
};