module.exports = {
	watch: false,
	module: {
		rules: [
			{ test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader' },
			{ test: /\.vue$/, loader: 'vue-loader' }
		]
	},
	resolve: {
		extensions: ['.js', '.vue'],
        alias: {
			'vue$': 'vue/dist/vue.esm.js'
		}
    }
};