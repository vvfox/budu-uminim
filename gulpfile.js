var gulp = require('gulp'),
		sass = require('gulp-sass'),
		autoprefixer = require('gulp-autoprefixer'),
		browserSync = require('browser-sync').create(),
		concat = require('gulp-concat'),
		rename = require('gulp-rename'),
		minifyCSS = require('gulp-clean-css'),
		minifyJS = require('gulp-uglify'),
		webpack = require('webpack'),
		webpackStream = require('webpack-stream'),
		named = require('vinyl-named'),
		tmpDir = 'www/local/templates/.default';


gulp.task('sass', function () {
	return gulp.src('src/sass/*.sass')
			.pipe(sass().on('error', sass.logError))
			.pipe(autoprefixer())
			.pipe(gulp.dest(tmpDir + '/css'));
});


gulp.task('minifyCSS', function () {
	return gulp.src([tmpDir + '/css/**/*.css', '!'+ tmpDir + '/css/**/*.min.css'])
			.pipe(minifyCSS())
			.pipe(rename({
				suffix: '.min'
			}))
			.pipe(gulp.dest(tmpDir + '/css'));
});

gulp.task('minifyJS', function () {
	return gulp.src([tmpDir + '/js/**/*.js', '!'+ tmpDir + '/js/**/*.min.js'])
			.pipe(minifyJS())
			.pipe(rename({
				suffix: '.min'
			}))
			.pipe(gulp.dest(tmpDir + '/js'));
});

gulp.task('build', ['sass', 'vendorJs', 'js'], function () {
	console.log('build succeeded');
});

gulp.task('minify', ['minifyCSS', 'minifyJS'], function () {
	console.log('minify succeeded');
});


gulp.task('js', function () {
	return gulp.src('src/js/entry/script.js')
			.pipe(named())
			.pipe(webpackStream(require('./webpack.config'), webpack))
			.pipe(gulp.dest(tmpDir + '/js'));
});

gulp.task('vendorJs', function () {
	return gulp.src([
		'src/lib/jquery/dist/jquery.js',
		'src/lib/some_old_school_lib.js',
		'src/lib/axios/dis/axios.js'
	])
			.pipe(concat('vendor.js'))
			.pipe(gulp.dest(tmpDir + '/js'));
});


gulp.task('watch', ['sass', 'vendorJs', 'js'], function () {
	var sassInPath = 'src/sass/*.sass',
			jsPath = ['src/js/**/*.js', 'src/js/**/*.vue'],
			mainJSPath = tmpDir + '/js/**/*.js',
			cssPath = tmpDir + '/css/**/*.css',
			allPath = ['src/**/*', '!src/sass/**/*', '!src/css/**/*', '!src/js/**/*', '!src/lib/**/*'];

	browserSync.init({
		proxy: 'budu.loc',
		notify: false,
		open: false
	});

	gulp.watch(sassInPath, ['sass']);
	gulp.watch(cssPath).on('change', browserSync.reload);
	gulp.watch(jsPath, ['js']);
	gulp.watch(mainJSPath).on('change', browserSync.reload);
	gulp.watch(allPath).on('change', browserSync.reload);
});